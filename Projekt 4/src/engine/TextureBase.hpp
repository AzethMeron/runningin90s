#ifndef TEXTURE_BASE
#define TEXTURE_BASE

#include <SFML/Graphics.hpp>
#include <memory>
#include <string>
#include <list>
#include <numeric>
#include "ImportedObject.hpp"
#include "logs.hpp"
#include "global_and_tools.hpp"

namespace ErrorsTextureBase
{
	const char PlaceholderOpening[] = "Critical error: Placeholder graphical file opening failure. Game might be unstable\n";
	const char FileOpening[] = "Error: Graphical file opening failure\n";
	const char ReturnPlaceholder[] = "Error: Returning placeholder\n";
} 

template <unsigned int Size>
class TextureBase
{
	private:
		std::list<ImportedObject<sf::Texture>> data[Size];
		std::shared_ptr<sf::Texture> placeholder;
	protected:
		void Add(const ImportedObject<sf::Texture>& input)
		{ data[Generator(input.name)].push_front(input); }
		void Load_Placeholder(const	std::string& filename)
		{
			if(!placeholder->loadFromFile(filename.c_str()))
			{ std::cerr << ErrorsTextureBase::PlaceholderOpening; }
		}
		unsigned int Generator(const std::string& chars) const
		{
			unsigned int output = 0;
			output = std::accumulate(chars.begin(),chars.end(), 1);
			output = output % Size;
			return output;
		}
		ImportedObject<sf::Texture> LoadFromFile(const std::string& filename) const
		{
			ImportedObject<sf::Texture> output;
			output.name = filename;
			if(!output.data->loadFromFile((std::string(Directionaries::TextureBase) + filename).c_str()))
			{
				std::cerr << ErrorsTextureBase::FileOpening;
				output.name = "Failure";
			}
			return output;
		}
	public:
		sf::Texture* Load(const std::string& filename)
		{
			// Searching in the base
			unsigned int index = Generator(filename);
			for(ImportedObject<sf::Texture>& Wsk : data[index])
			{
				if(Wsk.name == filename)
				{
					return &(*Wsk.data);
				}
			}
			// Not in base, loading
			ImportedObject<sf::Texture> temp = LoadFromFile(filename);
			if(temp.name == "Failure")
			{
				return &(*placeholder);
			}
			Add(temp);
			return &(*temp.data);
		}
		TextureBase(const std::string& filename) : placeholder(new sf::Texture)
		{
			this->Load_Placeholder(std::string(Directionaries::TextureBase) + filename);
		}
};

#endif
