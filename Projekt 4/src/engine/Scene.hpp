#ifndef SCENE_HPP
#define SCENE_HPP

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "Tile.hpp"
#include <vector>
#include <string>

/* ******************************************************************************************** */

template <unsigned int Dimensions>
class Scene
{
	protected:
		sf::RenderWindow& oknoAplikacji;
		std::vector< std::vector<Tile<Dimensions>> > data;
		std::string name;
	public:
		virtual std::string Loop(void) = 0;
		void resize(const unsigned int& size) { data.resize(size); }
		void Rename(const std::string& Name) { name = Name; }
		std::vector<Tile<Dimensions>>& operator [] (const unsigned int& Ind)       { return data[Ind]; }
		std::vector<Tile<Dimensions>>  operator [] (const unsigned int& Ind) const { return data[Ind]; }
		void Draw(void) const;
		std::string Name(void) const { return name; }
		unsigned int size(void) const { return data.size(); }
		sf::Vector2i MouseLeft(const sf::Event& event) const;
		sf::Vector2i MouseRight(const sf::Event& event) const;
		sf::Vector2i TileLeft(const sf::Event& event) const;
		sf::Vector2i TileRight(const sf::Event& event) const;
		void Clear(void) { oknoAplikacji.clear();}
		Scene(sf::RenderWindow& okno) : oknoAplikacji(okno) { }
};

template <unsigned int Dimensions>
void Scene<Dimensions>::Draw(void) const
{
	for(unsigned int i = 0; i < data.size(); i++)
	{
		for(unsigned int j = 0; j < data[i].size(); j++)
		{
			data[i][j].Draw(oknoAplikacji);
		}
	}
	oknoAplikacji.display();
}

template <unsigned int Dimensions>
sf::Vector2i Scene<Dimensions>::MouseLeft(const sf::Event& event) const
{
	if( event.type == sf::Event::MouseButtonPressed)
	{
		if (event.mouseButton.button == sf::Mouse::Left)
		{
			return sf::Mouse::getPosition( oknoAplikacji );
		}
	}
	throw false;
}

template <unsigned int Dimensions>
sf::Vector2i Scene<Dimensions>::MouseRight(const sf::Event& event) const
{
	if( event.type == sf::Event::MouseButtonPressed)
	{
		if (event.mouseButton.button == sf::Mouse::Right)
		{
			return sf::Mouse::getPosition( oknoAplikacji );
		}
	}
	throw false;
}

template <unsigned int Dimensions>
sf::Vector2i Scene<Dimensions>::TileLeft(const sf::Event& event) const
{
	sf::Vector2i temp;
	try {
		temp = MouseLeft(event);
	}
	catch(bool check) { if(check == false) throw false; }
	unsigned int x = 0;
	unsigned int y = 0;
	for(unsigned int i = 0; i < data.size(); i++)
	{
		x = i;
		if(temp.x < (data[i][0].LTop())[0] + data[i][0].X())
			break;
	}
	for(unsigned int i = 0; i < data[x].size(); i++)
	{
		y = i;
		if(temp.y < (data[x][i].LTop())[1] + data[x][i].Y())
			break;
	}
	temp.x = x;
	temp.y = y;
	return temp;
}

template <unsigned int Dimensions>
sf::Vector2i Scene<Dimensions>::TileRight(const sf::Event& event) const
{
	sf::Vector2i temp;
	try {
		temp = MouseRight(event);
	}
	catch(bool check) { if(check == false) throw false; }
	unsigned int x = 0;
	unsigned int y = 0;
	for(unsigned int i = 0; i < data.size(); i++)
	{
		x = i;
		if(temp.x < (data[i][0].LTop())[0] + data[i][0].X())
			break;
	}
	for(unsigned int i = 0; i < data[x].size(); i++)
	{
		y = i;
		if(temp.y < (data[x][i].LTop())[1] + data[x][i].Y())
			break;
	}
	temp.x = x;
	temp.y = y;
	return temp;
}

#endif
