#ifndef AUDIOTYPES_HPP
#define AUDIOTYPES_HPP

#include <SFML/Audio.hpp>

// OGG Vorbis
struct Sound
{
	sf::Sound SoundEffect;
	
	void load(const char File[])
	{	this->SoundEffect.setBuffer(*BaseOfSound.Load(File));	}
	void setBuffer(sf::SoundBuffer& Temp)
	{	this->SoundEffect.setBuffer(Temp);	}
	void play(void)
	{	this->SoundEffect.play();	}
	void stop(void)
	{	this->SoundEffect.stop();	}
	void pause(void)
	{	this->SoundEffect.pause();	}
	void setLoop(const bool& Temp)
	{	this->SoundEffect.setLoop(Temp);	}
	void setPitch(const double& Temp)
	{	this->SoundEffect.setPitch(Temp);	}
	void setVolume(const float& Temp)
	{	this->SoundEffect.setVolume(Temp);	}
	
	Sound()
	{		}
	Sound(const char File[])
	{	this->load(File);	}
};

// FLAC
struct Music
{
	sf::Music* Track;
		
	void play(void)
	{	this->Track->play();	}
	void open(const char File[])
	{	Track = BaseOfMusic.Load(File);	}
	void stop(void)
	{	this->Track->stop();	}
	void pause(void)
	{	this->Track->pause();	}
	void setLoop(const bool& Temp)
	{	this->Track->setLoop(Temp);	}
	void setPitch(const double& Temp)
	{	this->Track->setPitch(Temp);	}
	void setVolume(const float& Temp)
	{	this->Track->setVolume(Temp);	}
	
	Music() 
	{		}
	Music(const char File[])
	{	this->open(File);	} 
	Music& operator = (sf::Music* Temp)
	{	this->Track = Temp;	return *this; }
};

#endif
