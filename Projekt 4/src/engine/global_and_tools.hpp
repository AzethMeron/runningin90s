#ifndef GLOBAL_AND_TOOLS_HPP
#define GLOBAL_AND_TOOLS_HPP

#include <string>
#include <cstdlib>
#include <ctime>

#define INF 999999

const sf::Color Color[] = { 
	sf::Color::Black, // 0
	sf::Color::Yellow // 1
	};

namespace Directionaries
{
	/* Primary ones */
	const char Data[] = "data/";
	const char Workspace[] = "workspace/";
	/* Secondary, depends on primary */
	const std::string TextureBase = Data + std::string("graphics/");
	const std::string SoundBase = Data + std::string("sound/");
	const std::string MusicBase = Data + std::string("music/");
	const std::string DataBase = Data + std::string("database/");
	const std::string Saves = Workspace + std::string("saves/");
	const std::string Temp = Workspace + std::string("temp/");
	/* Thirdary? */
	const std::string Scenes = DataBase + std::string("scenes/");
	const std::string Gobjects = DataBase + std::string("gobjects/");
}

template <typename Type>
inline void swap(Type& a, Type& b)
{
	Type c = a;
	a = b;
	b = c;
}

inline unsigned int loss(const unsigned int& min, const unsigned int& max)
{
	return (rand()%(max-min+1)+min);
}

inline bool chance(const unsigned int& percentage)
{
	if(loss(1,100) < percentage)
	{
		return true;
	}
	return false;
}

#include "MusicBase.hpp"
#include "SoundBase.hpp"
#include "TextureBase.hpp"
MusicBase<100> BaseOfMusic("Makishima.flac");
SoundBase<100> BaseOfSound("sample.ogg");
TextureBase<100> BaseOfTexture("Tex.jpg");
double GameSpeed = 1;
// This is inside GeometricalObject.hpp, due to some problems
// GeometricalObjectBase<2,100> BaseOfGobjects("placeholder.txt");

#endif
