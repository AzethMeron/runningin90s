#ifndef LOGS_HPP
#define LOGS_HPP

#include <fstream>
#include <chrono>
#include <iostream>
#include <string>

namespace ErrorsErrorStream
{
	const char File[] = "Error: Failed to open stream\n"; 
}

class ErrorStream
{
	private:
		double nanoseconds(void) const { return std::chrono::duration_cast<std::chrono::nanoseconds>(Now() - Start).count(); }
		double miliseconds(void) const { return nanoseconds() * 1e-6; }
		std::chrono::high_resolution_clock::time_point Now(void) const { return std::chrono::high_resolution_clock::now(); }
	protected:
		std::chrono::high_resolution_clock::time_point Start;
	public:
		std::ofstream stream;
		double results(void) const { return miliseconds(); }
		ErrorStream(const char File[]);
		~ErrorStream();
};

ErrorStream::ErrorStream(const char File[])
{
	Start = Now();
	stream.open(File);
	if(!stream.is_open())
	{
		std::cerr << ErrorsErrorStream::File;
	}
}

ErrorStream::~ErrorStream()
{
	if(stream.is_open())
		stream.close();
}

template <typename Type>
ErrorStream& operator << (ErrorStream& stream, const Type& input)
{
	stream.stream << "[" << stream.results() << "ms] " << input << std::endl;
	return stream;
}

ErrorStream logs( (Directionaries::Temp + std::string("logs.txt")).c_str() );

#endif
