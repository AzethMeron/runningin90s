#ifndef MUSICBASE_HPP
#define MUSICBASE_HPP

#include <SFML/Audio.hpp>
#include <memory>
#include <string>
#include <list>
#include <numeric>
#include "ImportedObject.hpp"
#include "logs.hpp"
#include "global_and_tools.hpp"

namespace ErrorsMusicBase
{
	const char FileOpening[] = "Error: Music file opening failure\n";
	const char TooManyObjects[] = "Error: Tried to load over 256 music objects, while 256 is the limit\n";
	const char PlaceholderOpening[] = "Critical error: Placeholder music file opening failure. Game might be unstable\n";
	const char ReturnPlaceholder[] = "Error: Returning music placeholder\n";
}

template <unsigned int Size>
class MusicBase
{
	private:
		std::shared_ptr<sf::Music> placeholder;
		std::list<ImportedObject<sf::Music>> data[Size];
		unsigned int counter;
	protected:
		void Add(const ImportedObject<sf::Music>& input)
		{ counter++; data[Generator(input.name)].push_front(input); if(counter > 255) { logs << ErrorsMusicBase::TooManyObjects; } }
		void Load_Placeholder(const std::string& filename)
		{
			if(!placeholder->openFromFile(filename.c_str()))
			{logs << ErrorsMusicBase::PlaceholderOpening; }
		}
		unsigned int Generator(const std::string& chars) const
		{
			unsigned int output = 0;
			output = std::accumulate(chars.begin(),chars.end(), 1);
			output = output % Size;
			return output;
		}
		ImportedObject<sf::Music> LoadFromFile(const std::string& filename) const
		{
			ImportedObject<sf::Music> output;
			output.name = filename;
			if(!output.data->openFromFile((std::string(Directionaries::MusicBase) + filename).c_str()))
			{
				logs << ErrorsMusicBase::FileOpening;
				output.name = "Failure";
			}
			return output;
		}
	public:
		sf::Music* Load(const std::string& filename)
		{
			// Searching in the base
			unsigned int index = Generator(filename);
			for(ImportedObject<sf::Music>& Wsk : data[index])
			{
				if(Wsk.name == filename)
				{
					return &(*Wsk.data);
				}
			}
			
			// Not in base, loading
			ImportedObject<sf::Music> temp = LoadFromFile(filename);
			if(temp.name == "Failure")
			{
				return &(*placeholder);
			}
			Add(temp);
			return &(*temp.data);
		}
		MusicBase(const std::string& filename) : placeholder (new sf::Music)
		{
			Load_Placeholder(std::string(Directionaries::MusicBase) + filename);
			counter = 0;
		}
};

#endif
