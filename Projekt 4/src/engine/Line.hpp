#ifndef LINE_HPP
#define LINE_HPP

#include "Vector.hpp"

typedef TypeVector TypeLine;

template<unsigned int Dimensions>
class Line
{
	protected:
		Vector<Dimensions> Direction;
		Vector<Dimensions> Application;
	public:
		void CalculateLine(const Vector<Dimensions>& From, const Vector<Dimensions>& To);
		TypeLine Distance(const Vector<Dimensions>& temp) const;
		
		TypeLine Slope(void) const;
		TypeLine AbsoluteTerm(void) const;
		
		Line(const Vector<Dimensions>& From, const Vector<Dimensions>& To)
		{	this->CalculateLine(From,To);	}
};

template<unsigned int Dimensions>
void Line<Dimensions>::CalculateLine(const Vector<Dimensions>& From, const Vector<Dimensions>& To)
{
	Application = From;
	Direction = CalculateVector(From,To);
}

template<unsigned int Dimensions> // https://www.matematyka.pl/286010.htm
TypeLine Line<Dimensions>::Distance(const Vector<Dimensions>& temp) const
{
	TypeLine Output = 0;
	Vector<Dimensions> VectorTemp = CalculateVector(Direction,temp); 
	Output = (((Application.CrossProduct(VectorTemp)).Length())/(Application.Length()));
	return Output;
}

template<unsigned int Dimensions>  // Nachylenie
TypeLine Line<Dimensions>::Slope(void) const
{
	Vector<Dimensions> tmp = (Direction + Application);
	return tmp.Slope();
}

template<unsigned int Dimensions> 
TypeLine Line<Dimensions>::AbsoluteTerm(void) const // Wyraz Wolny
{
	return (Application.Value[1] - Slope()*Application.Value[0]);
}

#endif
