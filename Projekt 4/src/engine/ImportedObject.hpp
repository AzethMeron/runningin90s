#ifndef IMPORTED_OBJECT_HPP
#define IMPORTED_OBJECT_HPP

template <typename Type>
struct ImportedObject
{
	public:
		std::shared_ptr<Type> data;
		std::string name;
		
		ImportedObject() : data(new Type)
		{
			
		}
		ImportedObject(const ImportedObject& input)
		{
			data = input.data;
			name = input.name;
		}
};

#endif
