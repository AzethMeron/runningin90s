#ifndef SOUNDBASE_HPP
#define SOUNDBASE_HPP

#include <SFML/Audio.hpp>
#include <memory>
#include <string>
#include <list>
#include <numeric>
#include "ImportedObject.hpp"
#include "logs.hpp"
#include "global_and_tools.hpp"


namespace ErrorsSoundBase
{
	const char FileOpening[] = "Error: Sound file opening failure\n";
	const char TooManyObjects[] = "Error: Tried to load over 256 sound objects, while 256 is the limit\n";
	const char PlaceholderOpening[] = "Critical error: Placeholder sound file opening failure. Game might be unstable\n";
	const char ReturnPlaceholder[] = "Error: Returning sound placeholder\n";
}

template <unsigned int Size>
class SoundBase
{
	private:
		std::shared_ptr<sf::SoundBuffer> placeholder;
		std::list<ImportedObject<sf::SoundBuffer>> data[Size];
		unsigned int counter;
	protected:
		void Add(const ImportedObject<sf::SoundBuffer>& input)
		{ counter++; data[Generator(input.name)].push_front(input); if(counter > 255) { logs << ErrorsSoundBase::TooManyObjects; } }
		void Load_Placeholder(const std::string& filename)
		{
			if(!placeholder->loadFromFile(filename.c_str()))
			{logs << ErrorsSoundBase::PlaceholderOpening; }
		}
		unsigned int Generator(const std::string& chars) const
		{
			unsigned int output = 0;
			output = std::accumulate(chars.begin(),chars.end(), 1);
			output = output % Size;
			return output;
		}
		ImportedObject<sf::SoundBuffer> LoadFromFile(const std::string& filename) const
		{
			ImportedObject<sf::SoundBuffer> output;
			output.name = filename;
			if(!output.data->loadFromFile((std::string(Directionaries::SoundBase) + filename).c_str()))
			{
				logs << ErrorsSoundBase::FileOpening;
				output.name = "Failure";
			}
			return output;
		}
	public:
		sf::SoundBuffer* Load(const std::string& filename)
		{
			// Searching in the base
			unsigned int index = Generator(filename);
			for(ImportedObject<sf::SoundBuffer>& Wsk : data[index])
			{
				if(Wsk.name == filename)
				{
					return &(*Wsk.data);
				}
			}
			
			// Not in base, loading
			ImportedObject<sf::SoundBuffer> temp = LoadFromFile(filename);
			if(temp.name == "Failure")
			{
				return &(*placeholder);
			}
			Add(temp);
			return &(*temp.data);
		}
		SoundBase(const std::string& filename) : placeholder (new sf::SoundBuffer)
		{
			Load_Placeholder(std::string(Directionaries::SoundBase) + filename);
			counter = 0;
		}
};

#endif
