#ifndef TILE_HPP
#define TILE_HPP

#include <vector>
#include "GeometricalObject.hpp"
#include "Vector.hpp"

typedef TypeVector TypeTile;

template <unsigned int Dimensions>
class Tile
{
	protected:
		std::vector<GeometricalObject<Dimensions>> Objects;
		Vector<Dimensions> LeftTop;
		TypeTile SizeX;
		TypeTile SizeY;
		unsigned int wage;
	public:
		void push_back(const std::string& input);
		void push_back(const GeometricalObject<Dimensions>& input);
		void pop_back(void);
		void remove(const std::string& name);
		void remove(const GeometricalObject<Dimensions>& input);
		GeometricalObject<Dimensions>& operator [] (const unsigned int& Ind)       { return Objects[Ind]; }
		GeometricalObject<Dimensions>  operator [] (const unsigned int& Ind) const { return Objects[Ind]; }
		void Draw(sf::RenderWindow& Window) const;
		Vector<Dimensions> LTop(void) const { return LeftTop; }
		TypeTile Y(void) const { return SizeY; }
		TypeTile X(void) const { return SizeX; }
		unsigned int Wage(void) const { return this->wage; }
		bool If(const std::string& name_of_object) const;
		Tile(const Vector<Dimensions>& NewLeftTop, const unsigned int& Wage, const TypeTile& x, const TypeTile& y) 
		{ LeftTop = NewLeftTop; wage = Wage; SizeX = x; SizeY = y; }
};

template <unsigned int Dimensions>
void Tile<Dimensions>::push_back(const std::string& input)
{
	GeometricalObject<Dimensions> temp;
	temp.Load(input);
	temp.Move(LeftTop);
	Objects.push_back(temp);
}

template <unsigned int Dimensions>
void Tile<Dimensions>::push_back(const GeometricalObject<Dimensions>& input)
{
	GeometricalObject<Dimensions> temp = input;
	temp.Move(LeftTop);
	Objects.push_back(temp);
}

template <unsigned int Dimensions>
void Tile<Dimensions>::remove(const std::string& name)
{
	for(unsigned int i = 0; i < Objects.size()-1; i++)
	{
		if(Objects[i].Name() == name)
		{
			swap(Objects[i],Objects[i+1]);
		}
	}
	pop_back();
}

template <unsigned int Dimensions>
void Tile<Dimensions>::remove(const GeometricalObject<Dimensions>& input)
{
	for(unsigned int i = 0; i < Objects.size()-1; i++)
	{
		if(Objects[i] == input)
		{
			swap(Objects[i],Objects[i+1]);
		}
	}
	pop_back();
}

template <unsigned int Dimensions>
void Tile<Dimensions>::pop_back(void)
{
	Objects.pop_back();
}

template <unsigned int Dimensions>
void Tile<Dimensions>::Draw(sf::RenderWindow& Window) const
{
	for(unsigned int i = 0; i < Objects.size(); i++)
	{
		Objects[i].Draw(Window);
	}
}

template <unsigned int Dimensions>
std::istream& operator >> (std::istream& stream, Tile<Dimensions>& object)
{
	unsigned int number_of_objects;
	stream >> number_of_objects;
	for(unsigned int i = 0; i < number_of_objects; i++)
	{
		GeometricalObject<Dimensions> temp;
		stream >> temp;
		object.push_back(temp);
	}
	return stream;
}

template <unsigned int Dimensions>
bool Tile<Dimensions>::If(const std::string& name_of_object) const
{
	for(unsigned int i = 0; i < Objects.size(); i++)
	{
		if(name_of_object == Objects[i].Name())
		{
			return true;
		}
	}
	return false;
}

#endif
