#ifndef CLOCK_HPP
#define CLOCK_HPP

#include <chrono>

class Clock
{
	private:
		static std::chrono::high_resolution_clock::time_point Now(void) { return std::chrono::high_resolution_clock::now(); }
		double nanoseconds(void) const { return std::chrono::duration_cast<std::chrono::nanoseconds>(Current - Previous).count(); }
		double miliseconds(void) const { return nanoseconds() * 1e-6; }
	protected:
		double Framerate;
		std::chrono::high_resolution_clock::time_point Current;
		std::chrono::high_resolution_clock::time_point Previous;
	public:
		static void delay(const double& ms)
		{
			std::chrono::high_resolution_clock::time_point start = Now();
			std::chrono::high_resolution_clock::time_point stop = Now();
			while( ( std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count() * 1e-6 ) < ms)
			{
				stop = Now();
			}
		}
		void FramerateControl(void)
		{
			Current = Now();
			long double IdleTime = 1000.0 / Framerate;
			long double WorkTime = miliseconds();
			long double temp = IdleTime - WorkTime;
			if( temp > 0 )
			{
				if( IdleTime > 2*WorkTime) { Framerate = 1000.0 / WorkTime; }
				delay(temp);
			}
			else
			{
				Framerate = 1000.0 / miliseconds();
			}
			Previous = Current;
			GameSpeed = 60.0 / Framerate;
		}
		Clock()
		{
			Previous = Now();
			Framerate = 60;
		}
};

#endif
