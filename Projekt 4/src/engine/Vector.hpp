#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <cmath>

typedef float TypeVector;

template <unsigned int Dimensions>
class Vector
{
	private:
		
	protected:
		TypeVector Data[Dimensions];
	public:
		TypeVector& operator [] (const unsigned int& Ind)       { return Data[Ind]; }
		TypeVector  operator [] (const unsigned int& Ind) const { return Data[Ind]; }
		Vector<Dimensions>& operator = (const TypeVector& input);
		TypeVector Length(void) const;
		Vector<Dimensions>  operator + (const Vector<Dimensions>& input) const;
		Vector<Dimensions>  operator - (const Vector<Dimensions>& input) const;
		Vector<Dimensions>  operator * (const TypeVector& input) const;
		Vector<Dimensions>  operator / (const TypeVector& input) const;
		TypeVector DotProduct(const Vector<Dimensions>& input) const;
		Vector<3> CrossProduct(const Vector<Dimensions>& input) const;
		bool operator == (const Vector<Dimensions>& input) const;
		bool operator != (const Vector<Dimensions>& input) const;
		Vector<Dimensions> Versor(void) const;
		Vector<Dimensions> Opposite(void) const;
		
		Vector()
		{
			for(unsigned int i = 0; i < Dimensions; i++)
			{
				Data[i] = 0;
			}
		}
		Vector(const TypeVector& X, const TypeVector& Y)
		{
			Data[0] = X;
			Data[1] = Y;
		}
};

template <unsigned int Dimensions>
Vector<Dimensions>& Vector<Dimensions>::operator = (const TypeVector& input)
{
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		(*this)[i] = input;
	}
	return *this;
}

template <unsigned int Dimensions>
TypeVector Vector<Dimensions>::Length(void) const
{
	TypeVector Output = 0;
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		Output = Output + (*this)[i] * (*this)[i];
	}
	return sqrt(Output);
}

template <unsigned int Dimensions>
Vector<Dimensions>  Vector<Dimensions>::operator + (const Vector<Dimensions>& input) const
{
	Vector<Dimensions> Output;
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		Output[i] = (*this)[i] + input[i];
	}
	return Output;
}

template <unsigned int Dimensions>
Vector<Dimensions>  Vector<Dimensions>::operator - (const Vector<Dimensions>& input) const
{
	return ((*this) + input.Opposite());
}

template <unsigned int Dimensions>
Vector<Dimensions>  Vector<Dimensions>::operator * (const TypeVector& input) const
{
	Vector<Dimensions> Output;
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		Output[i] = (*this)[i] * input;
	}
	return Output;
}

template <unsigned int Dimensions>
Vector<Dimensions>  Vector<Dimensions>::operator / (const TypeVector& input) const
{
	Vector<Dimensions> Output;
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		Output[i] = (*this)[i] / input;
	}
	return Output;
}

template <unsigned int Dimensions>
TypeVector Vector<Dimensions>::DotProduct(const Vector<Dimensions>& input) const
{
	TypeVector Output = 0;
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		Output = Output + (*this)[i] * input[i];
	}
	return Output;
}

template <unsigned int Dimensions>
bool Vector<Dimensions>::operator == (const Vector<Dimensions>& input) const
{
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		if(fabs((*this)[i] - input[i])>0.0001)
		{
			return false;
		}
	}
	return true;
}

template <unsigned int Dimensions>
bool Vector<Dimensions>::operator != (const Vector<Dimensions>& input) const
{
	return !(*this == input);
}

template <unsigned int Dimensions>
Vector<Dimensions> Vector<Dimensions>::Versor(void) const
{
	return ((*this)/this->Length());
}

template <unsigned int Dimensions>
Vector<Dimensions> Vector<Dimensions>::Opposite(void) const
{
	Vector<Dimensions> Output;
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		Output[i] = -(*this)[i];
	}
	return Output;
}

#endif
