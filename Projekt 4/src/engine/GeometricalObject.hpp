#ifndef GEOMETRICALOBJECT_HPP
#define GEOMETRICALOBJECT_HPP

#include <string>
#include <vector>
#include "logs.hpp"
#include "Vector.hpp"
#include "Matrix.hpp"

/* ***************************************************************************************** */

typedef TypeVector TypeObject;

namespace ErrorsGeometricalObject
{
	const char Loading[] = "Error: Gobject loading failed\n";
}

template<unsigned int Dimensions>
class GeometricalObject
{
	protected:
		Vector<Dimensions> Center;
		std::vector<Vector<Dimensions>> Apex;
		sf::ConvexShape Shape;
		std::string name;
		inline void Create(void);	
	public:
		void Rotate(const int& AngleTimes10);
		
		void Place(const Vector<Dimensions>& NewCenter);
		void Place(const TypeObject& NewCenterX, const TypeObject& NewCenterY);
		void Move(const Vector<Dimensions>& input);
		
		void FillTexture(const char File[]);
		void FillColor(const sf::Color& Color);
		void Outline(const sf::Color& Color, const unsigned int& Thickness);
		void Scale(const unsigned int& Percentage);
		void Reshape(const std::vector<Vector<Dimensions>>& NewApexes);
		void Rename(const std::string& NewName) { name = NewName; }
		
		void Draw(sf::RenderWindow& Window) const;
		std::string Name(void) const { return name; }
		void Load(const std::string& name);
		
		bool operator == (const GeometricalObject<Dimensions>& input) const;
};

template<unsigned int Dimensions> //
inline void GeometricalObject<Dimensions>::Create(void)
{
	this->Shape.setPointCount(this->Apex.size());
	for(unsigned int i = 0; i < this->Apex.size(); i++)
	{
		TypeObject x = (this->Apex[i]+this->Center)[0];
		TypeObject y = (this->Apex[i]+this->Center)[1];
		this->Shape.setPoint(i,sf::Vector2f(x,y));
	}
}

template<unsigned int Dimensions> //
void GeometricalObject<Dimensions>::Rotate(const int& AngleTimes10)
{
	for(unsigned int i = 0; i < this->Apexes.Size(); i++)
	{
		Vector<Dimensions> Temp;
		for(unsigned int j = 0; j < Dimensions; j++)
		{	Temp[j] = this->Apexes[i][j];	}
		Matrix<Dimensions> Rotation;
		Rotation.Rotate2D(AngleTimes10);
		this->Apexes[i] = Rotation * Temp;
	}
	this->Create();
}

template<unsigned int Dimensions> //
void GeometricalObject<Dimensions>::Place(const Vector<Dimensions>& NewCenter)
{
	this->Center = NewCenter;
	this->Create();
}

template<unsigned int Dimensions> //
void GeometricalObject<Dimensions>::Place(const TypeObject& NewCenterX, const TypeObject& NewCenterY)
{
	Vector<Dimensions> NewCenter;
	NewCenter[0] = NewCenterX;
	NewCenter[1] = NewCenterY;
	this->Center = NewCenter;
	this->Create();
}

template<unsigned int Dimensions> //
void GeometricalObject<Dimensions>::Move(const Vector<Dimensions>& input)
{
	this->Center = this->Center + input;
	this->Shape.move(sf::Vector2f(input[0],input[1]));
	return;
}

template<unsigned int Dimensions> //
void GeometricalObject<Dimensions>::FillTexture(const char File[])
{
	this->Shape.setTexture((BaseOfTexture.Load(File)));
}

template<unsigned int Dimensions> //
void GeometricalObject<Dimensions>::FillColor(const sf::Color& Color)
{
	this->Shape.setFillColor(Color);
}

template<unsigned int Dimensions> //
void GeometricalObject<Dimensions>::Outline(const sf::Color& Color, const unsigned int& Thickness)
{
	this->Shape.setOutlineColor(Color);
	this->Shape.setOutlineThickness(Thickness);
}

template<unsigned int Dimensions> // 
void GeometricalObject<Dimensions>::Scale(const unsigned int& Percentage)
{
	for(unsigned int i = 0; i < this->Apexes.Size(); i++)
	{
		this->Apexes[i] = (double(this->Apexes[i] * Percentage))/(double(100));
	}
	this->Create();
}

template<unsigned int Dimensions> //
void GeometricalObject<Dimensions>::Reshape(const std::vector<Vector<Dimensions>>& NewApexes)
{
	this->Apex = NewApexes;
	this->Create();
}

template<unsigned int Dimensions> //
void GeometricalObject<Dimensions>::Draw(sf::RenderWindow& Window) const
{
	Window.draw((this->Shape));
}

template<unsigned int Dimensions> //
bool GeometricalObject<Dimensions>::operator == (const GeometricalObject<Dimensions>& input) const
{
	if(Name() == input.Name())
	{
		return true;
	}
	return false;
}

/* ***************************************************************************************** */

namespace ErrorsGeometricalObjectBase
{
	const char FileOpening[] = "Error: Gobject file opening failure\n";
	const char PlaceholderOpening[] = "Critical error: Placeholder gobject file opening failure. Game might be unstable\n";
	const char ReturnPlaceholder[] = "Error: Returning gobject placeholder\n";
}

template<unsigned int Dimensions, unsigned int Size>
class GeometricalObjectBase
{
	private:
		
	protected:
		std::list<GeometricalObject<Dimensions>> data[Size];
		GeometricalObject<Dimensions> placeholder;
	public:
		void Add(const GeometricalObject<Dimensions>& input)
		{
			data[Generator(input.Name())].push_front(input); 
		}
		void Load_Placeholder(const	std::string& filename)
		{
			std::ifstream stream;
			stream.open((std::string(filename)).c_str());
			if(stream.is_open())
			{
				stream >> placeholder;
			}
			else
			{
				logs << ErrorsGeometricalObjectBase::PlaceholderOpening;
			}
		}
		unsigned int Generator(const std::string& chars) const
		{
			unsigned int output = 0;
			output = std::accumulate(chars.begin(),chars.end(), 1);
			output = output % Size;
			return output;
		}
		GeometricalObject<Dimensions> LoadFromFile(const std::string& filename) const
		{
			std::ifstream stream;
			stream.open((Directionaries::Gobjects + std::string(filename) + std::string(".txt")).c_str());
			if(stream.is_open())
			{
				GeometricalObject<Dimensions> temp;
				stream >> temp;
				return temp;
			}
			else
			{
				logs << ErrorsGeometricalObjectBase::FileOpening;
				GeometricalObject<Dimensions> temp;
				temp.Rename("Failure");
				return temp;
			}
		}
	public:
		GeometricalObject<Dimensions> Load(const std::string& name)
		{
			// Searching in the base
			unsigned int index = Generator(name);
			for(GeometricalObject<Dimensions>& Wsk : data[index])
			{
				if(Wsk.Name() == name)
				{
					return Wsk;
				}
			}
			// Not in base, loading
			GeometricalObject<Dimensions> temp = LoadFromFile(name);
			if(temp.Name() == "Failure")
			{
				return placeholder;
			}
			Add(temp);
			return temp;
		}
		GeometricalObjectBase(const std::string& filename) 
		{
			this->Load_Placeholder(std::string(Directionaries::Gobjects) + filename);
		}
};

/* ***************************************************************************************** */

GeometricalObjectBase<2,100> BaseOfGobjects("placeholder.txt");

/* ***************************************************************************************** */

template<unsigned int Dimensions>
std::istream& operator >> (std::istream& stream, GeometricalObject<Dimensions>& object)
{
	bool old = false;
	stream >> old;
	if(old == true)
	{
		Vector<Dimensions> Center;
		for(unsigned int i = 0; i < Dimensions; i++)
		{
			stream >> Center[i];
		}
		
		unsigned int Apexes;
		stream >> Apexes;
		
		std::vector<Vector<Dimensions>> Apex(Apexes);
		for(unsigned int i = 0; i < Apexes; i++)
		{
			for(unsigned int j = 0; j < Dimensions; j++)
			{
				stream >> Apex[i][j];
			}
		}
		
		unsigned int outline = 0;
		stream >> outline;
		
		unsigned int oulinecolor = 0;
		stream >> oulinecolor;
		
		bool fill = false;
		stream >> fill;
		
		std::string tex;
		stream >> tex;
		
		std::string name;
		stream >> name;
		
		object.Reshape(Apex);
		object.Place(Center);
		object.Outline(Color[oulinecolor],outline);
		if(tex!="NONE") object.FillTexture(tex.c_str());
		if(fill==false) object.FillColor(sf::Color::Transparent);
		object.Rename(name);
		
		if(!stream.good())
		{
			logs << ErrorsGeometricalObject::Loading;
		}
	}
	else
	{
		std::string name;
		stream >> name;
		object = BaseOfGobjects.Load(name);
	}
	
	return stream;
}

template<unsigned int Dimensions>
void GeometricalObject<Dimensions>::Load(const std::string& name)
{
	*this = BaseOfGobjects.Load(name);
}

#endif
