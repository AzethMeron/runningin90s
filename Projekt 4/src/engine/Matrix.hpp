#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "Vector.hpp"
#include "logs.hpp"
#include "global_and_tools.hpp"

typedef TypeVector TypeMatrix;
#define ANGLE_TO_RADIANS 0.01745329

namespace ErrorsMatrix
{
	const char Determinant[] = "Error: Matrix's determinant equal zero\n";
}

/* **************************************************************************************** */

template <unsigned int Dimensions>
class MatrixMinusOne
{
	protected:
		Vector<Dimensions> Rows[Dimensions];
		void BringRowToZero(const unsigned int& Ind);
		bool ArrangeColumn(const unsigned int& Ind, TypeMatrix& Sign);
	public:
		Vector<Dimensions>  operator [] (const unsigned int& Ind) const { return Rows[Ind]; }
		Vector<Dimensions>& operator [] (const unsigned int& Ind)       { return Rows[Ind]; }
		Vector<Dimensions> operator * (const Vector<Dimensions>& input) const;
		MatrixMinusOne<Dimensions> operator * (const TypeMatrix& input) const;
		MatrixMinusOne<Dimensions> Transposed(void) const;
		TypeMatrix Determinant(void) const;
};

template <unsigned int Dimensions>
void MatrixMinusOne<Dimensions>::BringRowToZero(const unsigned int& Ind)
{
	Vector<Dimensions> temp;
	TypeMatrix Divisor = 0;
	for(unsigned int i = Ind + 1; i < Dimensions; i++)
	{
		temp=(*this)[Ind];
		Divisor = ((*this)[i][Ind]/(*this)[Ind][Ind]);
		(*this)[i] = ((*this)[i] - (temp * Divisor));
	}
}

template <unsigned int Dimensions>
bool MatrixMinusOne<Dimensions>::ArrangeColumn(const unsigned int& Ind, TypeMatrix& Sign)
{
	for(unsigned int i = Ind + 1; i < Dimensions; i++)
	{
		if(fabs((*this)[Ind][Ind])>0)
		{
			return true;
		}
		else
		{
			if(fabs((*this)[i][Ind])>0)
			{
				swap((*this)[i], (*this)[Ind]);
				Sign=(Sign*(-1));
				return true;
			}
		}
	}
	return false;
}

template <unsigned int Dimensions>
Vector<Dimensions> MatrixMinusOne<Dimensions>::operator * (const Vector<Dimensions>& input) const
{
	Vector<Dimensions> Output;
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		Output[i]=(*this)[i].DotProduct(input);
	}
	return Output;
}

template <unsigned int Dimensions>
MatrixMinusOne<Dimensions> MatrixMinusOne<Dimensions>::operator * (const TypeMatrix& input) const
{
	MatrixMinusOne<Dimensions> Output;
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		Output[i] = (*this)[i] * input;
	}
	return Output;
}

template <unsigned int Dimensions>
MatrixMinusOne<Dimensions> MatrixMinusOne<Dimensions>::Transposed(void) const
{
	MatrixMinusOne<Dimensions> Output;
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		for(unsigned int j = 0; j < Dimensions; j++)
		{
			Output[i][j] = (*this)[j][i];
		}
	}
	return Output;
}

template <unsigned int Dimensions>
TypeMatrix MatrixMinusOne<Dimensions>::Determinant(void) const
{
	MatrixMinusOne<Dimensions> Temp=*this;
	TypeMatrix Sign = 1, Output=1;
	
	for(unsigned int Ind = 0; Ind < Dimensions; Ind++)
	{
		Temp.ArrangeColumn(Ind, Sign);
		Temp.BringRowToZero(Ind);
	} 
	
	for(unsigned int Ind = 0; Ind < Dimensions; Ind++)
	{
		Output=((*this)[Ind][Ind] * Output);
	}
	
	Output=Output*Sign;
	
	return Output;
}

/* **************************************************************************************** */

template <unsigned int Dimensions> 
class Matrix : public MatrixMinusOne<Dimensions>
{
	protected:
		Matrix<Dimensions> Completion(void) const;
		MatrixMinusOne<Dimensions-1> Erase(const unsigned int& x, const unsigned int& y) const;
	public:
		Matrix<Dimensions> Rotate2D(const float& AngleTimes10);
		Matrix<Dimensions> Inversed(void) const;
		Matrix<Dimensions>& operator = (const MatrixMinusOne<Dimensions>& Temp);
		Matrix<Dimensions> Transposed(void) const;
		
		Matrix()
		{
			
		}
		
		Matrix(const MatrixMinusOne<Dimensions>& input)
		{
			(*this) = input;
		}
};

template <unsigned int Dimensions> 
Matrix<Dimensions> Matrix<Dimensions>::Completion(void) const
{
	Matrix<Dimensions> Output;
	MatrixMinusOne<Dimensions-1> Temp;
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		for(unsigned int j = 0; j < Dimensions; j++)
		{
			Temp = this->Erase(j,i);
			Output[i][j] = Temp.Determinant();
			if((j%2+i%2)%2)
			{
				Output[i][j] = Output[i][j]*(-1);
			}
		}
	}
	return Output;
}

template <unsigned int Dimensions> 
MatrixMinusOne<Dimensions-1> Matrix<Dimensions>::Erase(const unsigned int& x, const unsigned int& y) const
{
	// i~x~k
	// j~y~l
	MatrixMinusOne<Dimensions-1> Output;
	unsigned int l = 0;
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		if(i!=x)
		{
			unsigned int k = 0;
			for(unsigned int j = 0; j < Dimensions; j++)
			{
				if(j!=y)
				{
					Output[k][l] = (*this)[i][j];
					k++;
				}
			}
			l++;
		}
	}
	return Output;
}

template <unsigned int Dimensions> 
Matrix<Dimensions> Matrix<Dimensions>::Rotate2D(const float& AngleTimes10)
{
	(*this)[0][0]=cos(double(AngleTimes10)/10.0*ANGLE_TO_RADIANS);
	(*this)[0][1]=-sin(double(AngleTimes10)/10.0*ANGLE_TO_RADIANS);
	(*this)[1][0]=sin(double(AngleTimes10)/10.0*ANGLE_TO_RADIANS);
	(*this)[1][1]=cos(double(AngleTimes10)/10.0*ANGLE_TO_RADIANS);
	return *this;
}

template <unsigned int Dimensions> 
Matrix<Dimensions> Matrix<Dimensions>::Inversed(void) const
{
	/* Declaration of alterable values */
	Matrix<Dimensions> Output;
	TypeMatrix Factor;
	
	/* Calculating Factor */
	if(fabs(this->Determinant())<0.0001)
	{	logs<<ErrorsMatrix::Determinant; 	}
	Factor = 1.0 / this->Determinant();
	
	/* Calculating Completion */
	Output = this->Completion();
	
	/* Transposing */
	Output = Output.Transposed();
	
	/* Finalization */
	Output = Output * Factor;
	return Output;	
}

template <unsigned int Dimensions> 
Matrix<Dimensions>& Matrix<Dimensions>::operator = (const MatrixMinusOne<Dimensions>& Temp)
{
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		for(unsigned int j = 0; j < Dimensions; j++)
		{
			(*this)[i][j] = Temp[i][j];
		}
	}
	return *this;
}

template <unsigned int Dimensions> 
Matrix<Dimensions> Matrix<Dimensions>::Transposed(void) const
{
	Matrix<Dimensions> Output;
	for(unsigned int i = 0; i < Dimensions; i++)
	{
		for(unsigned int j = 0; j < Dimensions; j++)
		{
			Output[i][j] = (*this)[j][i];
		}
	}
	return Output;
}

/* **************************************************************************************** */

template <unsigned int Dimensions>
Vector<3> Vector<Dimensions>::CrossProduct(const Vector<Dimensions>& temp) const
{
	Vector<3> Output;
	if((Dimensions==2)||(Dimensions==3))
	{
		Vector<3> Normalised1, Normalised2;
		for(unsigned int i = 0; i < Dimensions; i++)
		{
			Normalised1[i] = (*this)[i];
			Normalised2[i] = temp[i];
		}
		Matrix<3> Jacobi;
		Jacobi[0] = 0;
		Jacobi[1] = Normalised1;
		Jacobi[2] = Normalised2;
		for(unsigned int i = 0; i < 3; i++)
		{
			MatrixMinusOne<2> TEMP = Jacobi.Erase(0,i);
			Output[i] = TEMP.Determinant();
		}
	}
	// http://matematyka.sosnowiec.pl/teoria26.html
	return Output;
}

/* **************************************************************************************** */

#endif
