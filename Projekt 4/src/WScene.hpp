#ifndef WSCENE_HPP
#define WSCENE_HPP

#include <iostream>
#include <cmath>

// Do Sceny jestem zadowolony z hierarchii klas i dziedziczenia. Powyżej - niezbyt, ale nie mam czasu na masterowanie
// Później dodam klasę Window, w której będzie wektor wskaźników na Scene, sama Scena będzie zaś bardziej opierać się na virtualkach
// tak aby dla np. planszy, menu itd. były różne klasy dziedziczące po Scenie, i ich metody byly wywoływane przez polimorfie
class WScene : public Scene<2>
{
	private:
		/* Rozgrywka */
		std::string AI(void);
		std::string Gracz(void);
		int VerifyMove(std::vector<sf::Vector2i> ruch, bool who) const;
		std::vector<sf::Vector2i> AnalizujGlobalnie(bool who) const;
		bool AnalizujLokalnie(bool who, sf::Vector2i punkt) const;
		bool PoleWolne(sf::Vector2i punkt) const;
		std::string twoj_pion(bool who) const { if(who) { return std::string("BialyPion"); } else { return std::string("CzarnyPion"); } }
		void DekrementacjaFigur(bool who);
		bool KoniecGry(void) const;
		
		/* Podstawowe informacje */
		bool player; // true - biale
		bool current_player;
		int biale;
		int czarne;
		
		/* Obsluga kursora */
		GeometricalObject<2> kursor;
		bool kursor_i;
		bool kursor_hardlock;
		sf::Vector2i kursor_p;
		
	protected:
		std::string Plansza(void);
		
	public:
		std::string Loop(void) override;
		void SetPlayer(bool if_white) { player = if_white; }
		WScene(sf::RenderWindow& okno) : Scene(okno)
		{ 
			current_player = true; 
			kursor.Load("Kursor"); 
			kursor_i = false; 
			kursor_hardlock = false;
			biale = 12;
			czarne = 12;
		}
};

std::string WScene::AI(void)
{
	player = !player;
	return name;
}

bool operator == (const sf::Vector2i& vec, const std::vector<sf::Vector2i>& lista)
{
	if(lista.size()==0) return true;
	for(unsigned int i = 0; i < lista.size(); i++)
	{
		if(vec == lista[i])
			return true;
	}
	return false;
}

std::string WScene::Gracz(void)
{
	sf::Event zdarzenie;
	while( oknoAplikacji.pollEvent(zdarzenie) )
	{
		/* Obsluga lewego przycisku myszy */
		if(sf::Event::MouseButtonPressed == zdarzenie.type)
		{
			/* Wykrywanie klikniecia */
			sf::Vector2i left; // Tutaj mamy namiary na Tile'a
			try {
				left = TileLeft(zdarzenie);
			}
			catch(bool check) { break;}
			/* Nakladanie Kursora */
			std::string pion = twoj_pion(player);
			if(data[left.x][left.y].If(pion))
			{
				if(kursor_hardlock == false)
				{
					std::vector<sf::Vector2i> temp = AnalizujGlobalnie(player);
					if(left == temp)
					{
						if(kursor_i == true)
						{
							data[kursor_p.x][kursor_p.y].remove(kursor);
						} 
						kursor_i = true;
						kursor_p = left;
						data[left.x][left.y].push_back(kursor);
					}
				}
			}
			/* Jesli klikamy na pole, gdzie nie ma naszego piona */
			else
			{
				if(kursor_i == true) // Jesli pion wybrany
				{
					if((left.x%2 + left.y%2)%2) // Jesli czarne pole
					{
						if(PoleWolne(left))
						{
							/* Bardziej czytelnie bawie sie zmiennymi from i to */
							sf::Vector2i from = kursor_p;
							sf::Vector2i to = left;
							/* Sprawdzamy ruch */
							std::vector<sf::Vector2i> temp;
							temp.push_back(from);
							temp.push_back(to);
							if(VerifyMove(temp,player) == 1) // Przesuniecie
							{
								if(kursor_hardlock == false)
								{
									std::vector<sf::Vector2i> temp = AnalizujGlobalnie(player);
									if(!temp.size())
									{
										/* Przesuniecie pionka */
										data[from.x][from.y].remove(kursor);
										data[from.x][from.y].remove(pion);
										data[to.x][to.y].push_back(pion);
										kursor_i = false;
										current_player = !current_player; // koniec tury
									}
								}
							}
							else if(VerifyMove(temp,player) == 2) // Bicie
							{
								/* Przesuniecie pionka */
								data[from.x][from.y].remove(kursor);
								data[from.x][from.y].remove(pion);
								data[to.x][to.y].push_back(pion);
								/* Zniszczenie pionka wroga */
								int x = (from.x + to.x)/2;
								int y = (from.y + to.y)/2;
								std::string pion = twoj_pion(!player);
								data[x][y].remove(pion);
								if(!AnalizujLokalnie(player,to))
								{
									current_player = !current_player; // koniec tury jesli koniec bicia
									kursor_hardlock = false;
									kursor_i = false;
								}
								else
								{
									kursor_i = true;
									kursor_hardlock = true;
									data[to.x][to.y].push_back(kursor);
									kursor_p = to;
								}
								/* Dekrementacja zmiennych */
								DekrementacjaFigur(player);
							}
						}
					}
				}
			}
		}
	}
	return name;
}

int WScene::VerifyMove(std::vector<sf::Vector2i> ruch, bool who) const
{
	sf::Vector2i from = ruch[0];
	sf::Vector2i to = ruch[1];
	if(PoleWolne(to))
	{
		if( abs(int(from.x) - int(to.x)) == 1) 
		{
			int temp = 0;
			if(who == true) { temp = 1; }
			else { temp = -1; }
			if( (int(from.y) - int(to.y)) == temp)
			{
				return 1; // ruch mozliwy
			}
		}
		else if(abs(int(from.x) - int(to.x)) == 2)
		{
			if( abs(int(from.y) - int(to.y)) == 2)
			{
				std::string pion = twoj_pion(!who);
				int x = (int(from.x) + int(to.x))/2;
				int y = (int(from.y) + int(to.y))/2;
				if(data[x][y].If(pion))
				{
					return 2; // ruch mozliwy, bicie
				}
			}
		}
	}
	return 0; // ruch niemozliwy
}

std::vector<sf::Vector2i> WScene::AnalizujGlobalnie(bool who) const
{
	std::vector<sf::Vector2i> output;
	for(unsigned int i = 0; i < data.size(); i++)
	{
		for(unsigned int j = 0; j < data[i].size(); j++)
		{
			if(data[i][j].If(twoj_pion(who)))
			{
				if(AnalizujLokalnie(who,sf::Vector2i(i,j)))
				{
					output.push_back(sf::Vector2i(i,j));
				}
			}
		}
	}
	return output;
}

bool WScene::AnalizujLokalnie(bool who, sf::Vector2i punkt) const
{
	int x[2] = { punkt.x-2, punkt.x+2 };
	int y[2] = { punkt.y-2, punkt.y+2 };
	for(int i = 0; i < 2; i++)
	{
		for(int j = 0; j < 2; j++)
		{
			if(x[i] > -1 && x[i] < 8)
			{
				if(y[j] > -1 && y[j] < 8)
				{
					std::vector<sf::Vector2i> ruch;
					ruch.push_back(punkt);
					ruch.push_back(sf::Vector2i(x[i],y[j]));
					if(VerifyMove(ruch,who) == 2)
					{
						return true;
					}
				}
			}
		}
	}
	return false;
}

bool WScene::PoleWolne(sf::Vector2i punkt) const
{
	if(!data[punkt.x][punkt.y].If(twoj_pion(player))) // Jesli pole wolne
	{
		if(!data[punkt.x][punkt.y].If(twoj_pion(!player))) // Jesli pole wolne
		{
			return true;
		}
	}
	return false;
}

void WScene::DekrementacjaFigur(bool who)
{
	if(who)
	{
		czarne--;
	}
	else
	{
		biale--;
	}
}

bool WScene::KoniecGry(void) const
{
	if(biale < 0 || czarne < 0)
	{
		return true;
	}
	return false;
}

std::string WScene::Plansza(void)
{
	std::string output = name;
	if(player == current_player)
	{
		name = Gracz();
	}
	else
	{
		name = AI();
	}
	Draw();
	if(KoniecGry())
	{
		Clock::delay(10000);
		// output =
	}
	return output;
}

std::string WScene::Loop(void)
{
	std::string output;
	if(name == "Plansza")
		output = Plansza();
	return output;
}

std::istream& operator >> (std::istream& stream, WScene& temp)
{
	std::string name;
	stream >> name;
	temp.Rename(name);
	
	unsigned int x = 0;
	stream >> x;
	temp.resize(x);
	
	for(unsigned int i = 0; i < x; i++)
	{
		unsigned int y = 0;
		stream >> y;
		for(unsigned int j = 0; j < y; j++)
		{
			unsigned int wage = 0;
			stream >> wage;
			unsigned int SizeX = 0;
			stream >> SizeX;
			unsigned int SizeY = 0;
			stream >> SizeY;
			unsigned int TopX = 0;
			stream >> TopX; 
			unsigned int TopY = 0;
			stream >> TopY;
			Vector<2> Top(TopX,TopY);
			Tile<2> push(Top,wage,SizeX,SizeY);
			temp[i].push_back(push);
		}
	}
	return stream;
}

#endif
