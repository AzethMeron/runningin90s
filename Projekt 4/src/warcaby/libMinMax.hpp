#ifndef LIBMINMAX_HPP
#define LIBMINMAX_HPP

#define GLEBOKOSC 4

// utrata piona, damki, przegrana itd. to po prostu -wygrana, -damka etc.

namespace libMinMax
{
	/* Te wartosci sa stale */
	const float Wygrana = 1000;
	
	/* Te wartosci maja dodana opcje losowania */ 
	const float Pion = 20;
	const float Damka = 80;
	const float Sciana = 20; // bezpieczny przy scianie
	const float Sciana1 = 10; // minumum 2 kratki od sciany
	const float LiniaKoncowa = 60;
	
	/* Losowanie; loss( 0,WspLosowania/100.0 * STALA ) - WspPrzesuniecia*STALA */
	const float WspLosowania = 20.0; // procenty
	const float WspPrzesuniecia = 10.0; // procenty
}

#endif
