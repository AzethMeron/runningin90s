#ifndef THEDRZEWO_HPP
#define THEDRZEWO_HPP

template<typename Typ>
struct Drzewo
{
	Typ Dane;
	std::vector<Drzewo<Typ>> dzieci;
};

#endif
