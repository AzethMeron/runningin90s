#ifndef WSCENE_HPP
#define WSCENE_HPP

#include <iostream>
#include <cmath>
#include <queue>
#include "libMinMax.hpp"
#include "thedrzewo.hpp"

struct Pion // Tak naprawde powinno sie nazywac Pole, namieszalem
{
	bool czy_wolny;
	bool player;
	bool czy_damka;
	std::string nazwa;
	Pion(const Tile<2>& input)
	{
		czy_wolny = true;
		if(input.If("CzarnyPion"))
		{
			czy_wolny = false;
			player = false;
			czy_damka = false;
			nazwa = "CzarnyPion";
		}
		else if(input.If("CzarnaDamka"))
		{
			czy_wolny = false;
			player = false;
			czy_damka = true;
			nazwa = "CzarnaDamka";
		}
		else if(input.If("BialyPion"))
		{
			czy_wolny = false;
			player = true;
			czy_damka = false;
			nazwa = "BialyPion";
		}
		else if(input.If("BialaDamka"))
		{
			czy_wolny = false;
			player = true;
			czy_damka = true;
			nazwa = "BialaDamka";
		}
	}
};

/* ****************************************************************************************** */

/* Struktury stworzone dla Sztucznej Inteligencji */
struct sRuch1
{
	sf::Vector2i from;
	sf::Vector2i to;
	int wage;
	bool gracz;
	sRuch1() { wage = 0; }
};

struct sRuch // lista mozliwych ruchow
{
	std::vector<sRuch1> moves;
	inline int wage(const bool& gracz) const
	{
		int output = 0;
		for(unsigned int i = 0; i < moves.size(); i++)
		{
			if(moves[i].gracz == gracz) output += moves[i].wage;
			else output -= moves[i].wage;
		}
		return output;
	}
};

struct sPole
{
	int stan; // 0 pole wolne, 1,2 bialy pion,damka, -1,-2 czarny pion,damka
	sPole() { stan = 0; }
	
	inline bool gracz(void) const
	{
		if(stan > 0)
			return true;
		else
			return false;
	}
	
	inline int znak(void)
	{
		if(stan > 0)
			return -1;
		else
			return 1;
	}
};

struct sPlansza
{
	std::vector<std::vector<sPole>> data;
	int glebokosc;
	
	std::vector<sPole>  operator [] (const unsigned int& ind) const { return data[ind]; }
	std::vector<sPole>& operator [] (const unsigned int& ind)       { return data[ind]; }
	unsigned int size(void) const { return data.size(); }
	
	inline int czarne(void) const
	{
		int output = 0; 
		for(unsigned int i = 0; i < data.size(); i++)
		{
			for(unsigned int j = 0; j < data[i].size(); j++)
			{
				if(data[i][j].stan != 0)
				{
					if(data[i][j].stan < 0)
					{
						output++;
					}
				}
			}
		}
		return output;
	}
	
	inline int biale(void) const
	{
		int output = 0; 
		for(unsigned int i = 0; i < data.size(); i++)
		{
			for(unsigned int j = 0; j < data[i].size(); j++)
			{
				if(data[i][j].stan != 0)
				{
					if(data[i][j].stan > 0)
					{
						output++;
					}
				}
			}
		}
		return output;
	}
	
	sPlansza(const std::vector< std::vector<Tile<2>> >& input_data)
	{
		glebokosc = 0;
		data.resize(input_data.size());
		for(unsigned int i = 0; i < input_data.size(); i++)
		{
			data[i].resize(input_data[i].size());
			for(unsigned int j = 0; j <input_data[i].size(); j++)
			{
				Pion temp(input_data[i][j]);
				if(!temp.czy_wolny)
				{
					int znak = 1;
					int figura = 1;
					if(!temp.player) znak = -1;
					if(temp.czy_damka) figura = 2;
					data[i][j].stan = figura * znak;
				}
			}
		}
	}
	
	sPlansza operator + (const sRuch1& ruch) const;
};

/* ****************************************************************************************** */

// Do Sceny jestem zadowolony z hierarchii klas i dziedziczenia. Powyżej - niezbyt, ale nie mam czasu na masterowanie
// Później dodam klasę Window, w której będzie wektor wskaźników na Scene, sama Scena będzie zaś bardziej opierać się na virtualkach
// tak aby dla np. planszy, menu itd. były różne klasy dziedziczące po Scenie, i ich metody byly wywoływane przez polimorfie
class WScene : public Scene<2>
{
	public:
		/* Sztuczna Inteligencja */
		void WykonajRuch(const sRuch& ruch);
		sRuch AnalizujSytuacje(const sPlansza& plansza, const bool& gracz) const;
		std::vector<sRuch1> MozliweRuchy(const sPlansza& plansza, const bool& gracz) const;
		static inline int VerifyMove(const sPlansza& plansza, sRuch1 ruch, const bool& gracz); 
		static inline bool AnalizujLokalnie(const sPlansza& plansza, const sf::Vector2i& punkt, const bool& gracz);
		
	private:
		/* Rozgrywka */
		std::string AI(void);
		std::string Gracz(void);
		int VerifyMove(std::vector<sf::Vector2i> ruch, const bool& who) const; // vector MUSI miec rowno 2 pola - przed i po ruchu. Ta, again, namieszalem
		std::vector<sf::Vector2i> AnalizujGlobalnie(const bool& who) const;
		bool AnalizujLokalnie(const bool& who, const sf::Vector2i& punkt) const;
		bool PoleWolne(const sf::Vector2i& punkt) const;
		void DekrementacjaFigur(const bool& who);
		bool KoniecGry(void) const;
		void Damki(void);
		
		/* Podstawowe informacje */
		bool player; // true - biale
		bool current_player;
		int biale;
		int czarne;
		
		/* Obsluga kursora */
		GeometricalObject<2> kursor;
		bool kursor_i;
		bool kursor_hardlock;
		sf::Vector2i kursor_p;
		
	protected:
		std::string Plansza(void);
		
	public:
		std::string Loop(void) override;
		void SetPlayer(bool if_white) { player = if_white; }
		WScene(sf::RenderWindow& okno) : Scene(okno)
		{ 
			current_player = true; 
			kursor.Load("Kursor"); 
			kursor_i = false; 
			kursor_hardlock = false;
			biale = 12;
			czarne = 12;
		}
};

/* ****************************************************************************************** */

sPlansza sPlansza::operator + (const sRuch1& ruch) const
{
	sPlansza output = *this;
	const sf::Vector2i& from = ruch.from;
	const sf::Vector2i& to = ruch.to;
	if(WScene::VerifyMove(output, ruch, false) == 1)
	{
		output[to.x][to.y].stan = output[from.x][from.y].stan;
		output[from.x][from.y].stan = 0;
	}
	else // 2
	{
		/* Przesuniecie pionka */
		output[to.x][to.y].stan = output[from.x][from.y].stan;
		output[from.x][from.y].stan = 0;
		/* Zniszczenie pionka wroga */
		int x = to.x;
		int y = to.y;
		{
			sf::Vector2i droga = to - from;
			x = x - (droga.x/abs(droga.x));
			y = y - (droga.y/abs(droga.y));
		}
		//std::string pion = twoj_pion(!player);
		output[x][y].stan = 0;
	}
	if(WScene::AnalizujLokalnie(output,to,false)) output.glebokosc++;
	return output;
}

void WScene::WykonajRuch(const sRuch& ruch) // wykonuje ruch, ostatnia metoda do wywolania
{
	for(unsigned int i = 0; i < ruch.moves.size(); i++)
	{
		std::cout << ruch.moves[i].to.x << " " << ruch.moves[i].to.x <<" ";
		std::cout << ruch.moves[i].from.x << " " << ruch.moves[i].from.x ;
		std::vector<sf::Vector2i> temp (2);
		const sf::Vector2i& from = ruch.moves[i].from;
		const sf::Vector2i& to = ruch.moves[i].to;
		temp[0] = from;
		temp[1] = to;
		Pion wybrany(data[from.x][from.y]);
		if(VerifyMove(temp,!player) == 1) // ruch
		{
			data[from.x][from.y].remove(wybrany.nazwa);
			data[to.x][to.y].push_back(wybrany.nazwa);
		}
		else if(VerifyMove(temp,!player) == 2) // bicie
		{
			/* Przesuniecie pionka */
			data[from.x][from.y].remove(wybrany.nazwa);
			data[to.x][to.y].push_back(wybrany.nazwa);
			/* Zniszczenie pionka wroga */
			int x = to.x;
			int y = to.y;
			{
				sf::Vector2i droga = to - from;
				x = x - (droga.x/abs(droga.x));
				y = y - (droga.y/abs(droga.y));
			}
			Pion do_zniszcz(data[x][y]);
			data[x][y].remove(do_zniszcz.nazwa);
			/* Dekrementacja zmiennych */
			DekrementacjaFigur(!player);
		}
		else
		{
			std::cout << (VerifyMove(temp,!player));
		}
	}
}

std::vector<sRuch1> WScene::MozliweRuchy(const sPlansza& plansza, const bool& gracz) const
{
	for(unsigned int i = 0; i < plansza.size(); i++)
	{
		for(unsigned int j = 0; j < plansza[i].size(); j++)
		{
			std::cout << plansza[i][j].stan;
		}
		std::cout << std::endl;
	}
	std::vector<sRuch1> output;
	for(unsigned int i = 0; i < plansza.size(); i++) // Znajdowanie ruchow "wymuszonych" - bicia
	{
		for(unsigned int j = 0; j < plansza[i].size(); j++)
		{
			if(plansza[i][j].stan != 0)
			{
				if(plansza[i][j].gracz() == gracz)
				{
					const sf::Vector2i punkt (i,j);
					if(AnalizujLokalnie(plansza, punkt, gracz))
					{
						int I = 2;
						int J = 2;
						while(I < int(plansza.size()))
						{
							int x[2] = { int(punkt.x) - I, int(punkt.x) + I };
							int y[2] = { int(punkt.y) - J, int(punkt.y) + J };
							if(abs(plansza[punkt.x][punkt.y].stan)==1)
							{
								if(I > 2)
								{
									if(J > 2)
									{
										break;
									} 
								}
							}
							for(unsigned int k = 0; k < 2; k++)
							{
								for(unsigned int l = 0; l < 2; l++)
								{
									if(x[k] >= 0 && x[k] < 8)
									{
										if(y[l] >= 0 && y[l] < 8)
										{
											sRuch1 ruch1;
											ruch1.from = punkt;
											ruch1.to = (sf::Vector2i(x[k],y[l]));
											if(VerifyMove(plansza,ruch1,gracz) == 2)
											{
												ruch1.wage = 0;
												ruch1.gracz = gracz;
												sf::Vector2i from = ruch1.from, to = ruch1.to;
												int x = to.x; int y = to.y; { sf::Vector2i droga = to - from; x = x - (droga.x/abs(droga.x)); y = y - (droga.y/abs(droga.y)); }
												if(abs(plansza[x][y].stan) == 1) ruch1.wage += loss( 0,libMinMax::WspLosowania/100.0 * libMinMax::Pion ) - libMinMax::WspPrzesuniecia * libMinMax::Pion;
												if(abs(plansza[x][y].stan) == 2) ruch1.wage += loss( 0,libMinMax::WspLosowania/100.0 * libMinMax::Damka ) - libMinMax::WspPrzesuniecia * libMinMax::Damka;
												if(ruch1.to.x == 0 || ruch1.to.x == 7) ruch1.wage += loss( 0,libMinMax::WspLosowania/100.0 * libMinMax::Sciana ) - libMinMax::WspPrzesuniecia * libMinMax::Sciana;
												if(ruch1.to.x > 1 && ruch1.to.x < 6) ruch1.wage += loss( 0,libMinMax::WspLosowania/100.0 * libMinMax::Sciana1 ) - libMinMax::WspPrzesuniecia * libMinMax::Sciana1;
												if(ruch1.to.y == 0 || ruch1.to.y == 7) ruch1.wage += loss( 0,libMinMax::WspLosowania/100.0 * libMinMax::LiniaKoncowa ) - libMinMax::WspPrzesuniecia * libMinMax::LiniaKoncowa;
												if(plansza.biale()-1 <= 0) ruch1.wage += libMinMax::Wygrana;
												output.push_back(ruch1);
												std::cout << "RUCH"<<ruch1.from.x;
											}
										}
									}
								}
							}
							I++;
							J++;
						}
					}
				}
			}
		}
	}
	if(!output.size()) // jesli nie ma ruchow "wymuszonych" - zwykly ruch
	{
		for(unsigned int i = 0; i < plansza.size(); i++) 
		{
			for(unsigned int j = 0; j < plansza[i].size(); j++)
			{
				if(plansza[i][j].stan != 0)
				{
					if(plansza[i][j].gracz() == gracz)
					{
						sf::Vector2i punkt (i,j);
						int I = 1;
						int J = 1;
						while(I < int(plansza.size()))
						{
							int x[2] = { int(punkt.x) - I, int(punkt.x) + I };
							int y[2] = { int(punkt.y) - J, int(punkt.y) + J };
							if(abs(plansza[punkt.x][punkt.y].stan)==1)
							{
								if(I > 2)
								{
									if(J > 2)
									{
										break;
									} 
								}
							}
							for(unsigned int k = 0; k < 2; k++)
							{
								for(unsigned int l = 0; l < 2; l++)
								{
									if(x[k] >= 0 && x[k] < 8)
									{
										if(y[l] >= 0 && y[l] < 8)
										{
											sRuch1 ruch1;
											ruch1.from = punkt;
											ruch1.to = (sf::Vector2i(x[k],y[l]));
											if(VerifyMove(plansza,ruch1,gracz) == 1)
											{
												ruch1.wage = 0;
												ruch1.gracz = gracz;
												if(ruch1.to.x == 0 || ruch1.to.x == 7) ruch1.wage += loss( 0,libMinMax::WspLosowania/100.0 * libMinMax::Sciana ) - libMinMax::WspPrzesuniecia * libMinMax::Sciana;
												if(ruch1.to.x > 1 && ruch1.to.x < 6) ruch1.wage += loss( 0,libMinMax::WspLosowania/100.0 * libMinMax::Sciana1 ) - libMinMax::WspPrzesuniecia * libMinMax::Sciana1;
												if(ruch1.to.y == 0 || ruch1.to.y == 7) ruch1.wage += loss( 0,libMinMax::WspLosowania/100.0 * libMinMax::LiniaKoncowa ) - libMinMax::WspPrzesuniecia * libMinMax::LiniaKoncowa;
												output.push_back(ruch1);
											}
										}
									}
								}
							}
							I++;
							J++;
							if(abs(plansza[i][j].stan)==1) break;
						}
					}
				}
			}
		}
	}
	else
	{
		std::cout << "BICIEEEEEEEE";
	}
	return output;
}

sRuch WScene::AnalizujSytuacje(const sPlansza& plansza, const bool& gracz) const // przeprowadza symulacje 4 nastepnych ruchow obydwu graczy i wybiera optymalny ruch (rekurencyjnie)
{
	sRuch output;
	if( plansza.glebokosc < GLEBOKOSC )
	{
		int wage = -10;
		int ind = 0;
		std::vector<sRuch1> Mozliwe_Ruchy = MozliweRuchy(plansza,gracz);
		Drzewo<std::vector<sRuch1>> korzen;
		
		std::cout << Mozliwe_Ruchy.size() << std::endl;
		for(unsigned int i = 0; i < Mozliwe_Ruchy.size(); i++)
		{
			if(Mozliwe_Ruchy[i].wage >= wage)
			{
				ind = i;
				wage = Mozliwe_Ruchy[i].wage;
			}
		}
		output.moves.push_back(Mozliwe_Ruchy[ind]);
		
		/* Analizujemy mozliwe ruchy, zliczamy ich wagi
		 * std::vector<sRuch1> - wektor możliwych ruchów
		 * Jesli glebokosc planszy > 3, zwraca std::vector<sRuch1> size = 0
		 * Jesli glebokosc planszy <1,3> zwraca std::vector<sRuch1> size = 1 + 3 - i, gdzie i to glebokosc;
		 * Jesli glebokosc planszy == 0, zwraca sRuch, size w zaleznosci od bicia/wielobicia
		 * Metoda wywoluje sie rekurencyjnie, dla (plansza+sRuch1) i składa z tego kolejke priorytetową z której wybiera najniższą wagę
		 * i ją zwraca
		 */
	}
	return output;
}

int WScene::VerifyMove(const sPlansza& plansza, sRuch1 ruch, const bool& gracz) // sprawdza czy wykonanie danego ruchu jest K
{
	const sPole& start = plansza[ruch.from.x][ruch.from.y];
	const sPole& cel = plansza[ruch.to.x][ruch.to.y];
	const sf::Vector2i droga = ruch.to - ruch.from;
	if(start.stan != 0)
	{
		if(cel.stan == 0) 
		{
			if(abs(droga.x)==abs(droga.y)) // Jesli ruch po skosie
			{
				int dx = droga.x/abs(droga.x);
				int dy = droga.y/abs(droga.y);
				for(int i = 0; i < abs(droga.x); i++) // analiza w jedna strone - czy po drodze nie ma sojuszniczych figur
				{
					ruch.from.x += dx;
					ruch.from.y += dy;
					const sPole& analizowany_punkt = plansza[ruch.from.x][ruch.from.y];
					if(analizowany_punkt.stan != 0)
					{
						if(analizowany_punkt.gracz() == gracz)
						{
							return 0;
						}
					}
				}
				bool bicie = false; // flaga bicie, domyslnie false
				for(int i = abs(droga.x); i > 1; i--) // analiza w druga strone - detekcja bicia/poruszenia. Wykonuje sie raz mniej
				{
					ruch.from.x -= dx;
					ruch.from.y -= dy;
					const sPole& analizowany_punkt = plansza[ruch.from.x][ruch.from.y];
					if(i != abs(droga.x)) // glowna petla
					{
						if((analizowany_punkt.stan!=0))
						{
							return 0;
						}
					}
					else // wykona sie raz
					{
						if((analizowany_punkt.stan!=0))
						{
							bicie = true;
						}
					}
				}
				if(abs(droga.x) > 2)
				{
					if(abs(start.stan)==2)
					{
						return (1+int(bicie));
					}
				}
				else if(abs(droga.x) == 2)
				{
					if(bicie)
					{
						return 2;
					}
				}
				else if(abs(droga.x) == 1)
				{
					if(abs(start.stan)==2)
					{
						return 1;
					}
					else
					{
						if(start.gracz())
						{
							if(droga.y == -1)
							{
								return 1;
							}
						}
						else
						{
							if(droga.y == 1)
							{
								return 1;
							}
						}
					}
				}
			}
		}
	}
	return 0; // ruch niemozliwy
}

bool WScene::AnalizujLokalnie(const sPlansza& plansza, const sf::Vector2i& punkt, const bool& gracz) 
// sprawdza mozliwosc do bicia danego piona
// zrobilem osobna metode z powodu slabej optymalizacji tej dla gracza
{
	const sPole& adres = plansza[punkt.x][punkt.y];
	int i = 2;
	int j = 2;
	while(i < int(plansza.size()))
	{
		int x[2] = { int(punkt.x) - i, int(punkt.x) + i };
		int y[2] = { int(punkt.y) - j, int(punkt.y) + j };
		if(abs(adres.stan)<=1)
		{
			if(i > 2)
			{
				if(j > 2)
				{
					return false;
				} 
			}
		}
		for(unsigned int k = 0; k < 2; k++)
		{
			for(unsigned int l = 0; l < 2; l++)
			{
				if(x[k] >= 0 && x[k] < 8)
				{
					if(y[l] >= 0 && y[l] < 8)
					{
						sRuch1 ruch1;
						ruch1.from = punkt;
						ruch1.to = (sf::Vector2i(x[k],y[l]));
						if(VerifyMove(plansza,ruch1,gracz) == 2)
						{
							return true;
						}
					}
				}
			}
		}
		i++;
		j++;
	}
	return false;
}

std::string WScene::AI(void)
{
	sPlansza aktualny_stan_planszy(data);
	WykonajRuch(AnalizujSytuacje(aktualny_stan_planszy,!player));
	current_player = !current_player;
	return name;
}

/* ****************************************************************************************** */

bool operator == (const sf::Vector2i& vec, const std::vector<sf::Vector2i>& lista)
{
	if(lista.size()==0) return true;
	for(unsigned int i = 0; i < lista.size(); i++)
	{
		if(vec == lista[i])
			return true;
	}
	return false;
}

std::string WScene::Gracz(void)
{
	sf::Event zdarzenie;
	while( oknoAplikacji.pollEvent(zdarzenie) )
	{
		/* Obsluga lewego przycisku myszy */
		if(sf::Event::MouseButtonPressed == zdarzenie.type)
		{
			std::vector<sf::Vector2i> temp = AnalizujGlobalnie(player);
			/* Wykrywanie klikniecia */
			sf::Vector2i left; // Tutaj mamy namiary na Tile'a
			try {
				left = TileLeft(zdarzenie);
			}
			catch(bool check) { break;}
			/* Nakladanie Kursora */
			Pion l_klikniecie(data[left.x][left.y]);
			if(l_klikniecie.player == player)
			{
				if(kursor_hardlock == false)
				{
					if(left == temp)
					{
						if(kursor_i == true)
						{
							data[kursor_p.x][kursor_p.y].remove(kursor);
						} 
						kursor_i = true;
						kursor_p = left;
						data[left.x][left.y].push_back(kursor);
					}
				}
			}
			/* Jesli klikamy na pole, gdzie nie ma naszego piona */
			else
			{
				Pion l_kursor(data[kursor_p.x][kursor_p.y]);
				if(kursor_i == true) // Jesli pion wybrany
				{
					if((left.x%2 + left.y%2)%2) // Jesli czarne pole
					{
						if(PoleWolne(left))
						{
							/* Bardziej czytelnie bawie sie zmiennymi from i to */
							sf::Vector2i from = kursor_p;
							sf::Vector2i to = left;
							/* Sprawdzamy ruch */
							std::vector<sf::Vector2i> ruch;
							ruch.push_back(from);
							ruch.push_back(to);
							if(VerifyMove(ruch,player) == 1) // Przesuniecie
							{
								if(kursor_hardlock == false)
								{
									if(!temp.size())
									{
										/* Przesuniecie pionka */
										data[from.x][from.y].remove(kursor);
										data[from.x][from.y].remove(l_kursor.nazwa);
										data[to.x][to.y].push_back(l_kursor.nazwa);
										kursor_i = false;
										current_player = !current_player; // koniec tury
									}
								}
							}
							else if(VerifyMove(ruch,player) == 2) // Bicie
							{
								/* Przesuniecie pionka */
								data[from.x][from.y].remove(kursor);
								data[from.x][from.y].remove(l_kursor.nazwa);
								data[to.x][to.y].push_back(l_kursor.nazwa);
								/* Zniszczenie pionka wroga */
								int x = to.x;
								int y = to.y;
								{
									sf::Vector2i droga = to - from;
									x = x - (droga.x/abs(droga.x));
									y = y - (droga.y/abs(droga.y));
								}
								//std::string pion = twoj_pion(!player);
								Pion do_zniszcz(data[x][y]);
								data[x][y].remove(do_zniszcz.nazwa);
								if(!AnalizujLokalnie(player,to))
								{
									current_player = !current_player; // koniec tury jesli koniec bicia
									kursor_hardlock = false;
									kursor_i = false;
								}
								else
								{
									kursor_i = true;
									kursor_hardlock = true;
									data[to.x][to.y].push_back(kursor);
									kursor_p = to;
								}
								/* Dekrementacja zmiennych */
								DekrementacjaFigur(player);
							}
						}
					}
				}
			}
		}
	}
	return name;
}

int WScene::VerifyMove(std::vector<sf::Vector2i> ruch, const bool& who) const
{
	Pion start(data[ruch[0].x][ruch[0].y]);
	Pion cel(data[ruch[1].x][ruch[1].y]);
	const sf::Vector2i droga = ruch[1] - ruch[0];
	if(cel.czy_wolny) 
	{
		if(abs(droga.x)==abs(droga.y)) // Jesli ruch po skosie
		{
			int dx = droga.x/abs(droga.x);
			int dy = droga.y/abs(droga.y);
			for(int i = 0; i < abs(droga.x); i++) // analiza w jedna strone - czy po drodze nie ma sojuszniczych figur
			{
				ruch[0].x += dx;
				ruch[0].y += dy;
				Pion analizowany_punkt(data[ruch[0].x][ruch[0].y]);
				if(!analizowany_punkt.czy_wolny)
				{
					if(analizowany_punkt.player == who)
					{
						return 0;
					}
				}
			}
			bool bicie = false; // flaga bicie, domyslnie false
			for(int i = abs(droga.x); i > 1; i--) // analiza w druga strone - detekcja bicia/poruszenia. Wykonuje sie raz mniej
			{
				ruch[0].x -= dx;
				ruch[0].y -= dy;
				Pion analizowany_punkt(data[ruch[0].x][ruch[0].y]);
				if(i != abs(droga.x)) // glowna petla
				{
					if(!(analizowany_punkt.czy_wolny))
					{
						return 0;
					}
				}
				else // wykona sie raz
				{
					if(!(analizowany_punkt.czy_wolny))
					{
						bicie = true;
					}
				}
			}
			if(abs(droga.x) > 2)
			{
				if(start.czy_damka)
				{
					return (1+int(bicie));
				}
			}
			else if(abs(droga.x) == 2)
			{
				if(bicie)
				{
					return 2;
				}
			}
			else if(abs(droga.x) == 1)
			{
				if(start.czy_damka)
				{
					return 1;
				}
				else
				{
					if(start.player)
					{
						if(droga.y == -1)
						{
							return 1;
						}
					}
					else
					{
						if(droga.y == 1)
						{
							return 1;
						}
					}
				}
			}
		}
	}
	return 0; // ruch niemozliwy
}

std::vector<sf::Vector2i> WScene::AnalizujGlobalnie(const bool& who) const
{
	std::vector<sf::Vector2i> output;
	for(unsigned int i = 0; i < data.size(); i++)
	{
		for(unsigned int j = 0; j < data[i].size(); j++)
		{
			Pion Wybrany(data[i][j]);
			if(!Wybrany.czy_wolny)
			{
				if(Wybrany.player == who)
				{
					if(AnalizujLokalnie(who,sf::Vector2i(i,j)))
					{
						output.push_back(sf::Vector2i(i,j));
					}
				}
			}
		}
	}
	return output;
}

bool WScene::AnalizujLokalnie(const bool& who, const sf::Vector2i& punkt) const
{
	Pion tutaj(data[punkt.x][punkt.y]);
	int i = 2;
	int j = 2;
	while(i < int(data.size()))
	{
		int x[2] = { int(punkt.x) - i, int(punkt.x) + i };
		int y[2] = { int(punkt.y) - j, int(punkt.y) + j };
		if(!tutaj.czy_damka)
		{
			if(i > 2)
			{
				if(j > 2)
				{
					return false;
				} 
			}
		}
		for(unsigned int k = 0; k < 2; k++)
		{
			for(unsigned int l = 0; l < 2; l++)
			{
				if(x[k] >= 0 && x[k] < 8)
				{
					if(y[l] >= 0 && y[l] < 8)
					{
						std::vector<sf::Vector2i> ruch;
						ruch.push_back(punkt);
						ruch.push_back(sf::Vector2i(x[k],y[l]));
						if(VerifyMove(ruch,who) == 2)
						{
							return true;
						}
					}
				}
			}
		}
		i++;
		j++;
	}
	return false;
}

bool WScene::PoleWolne(const sf::Vector2i& punkt) const
{
	Pion temp(data[punkt.x][punkt.y]);
	if(temp.czy_wolny)
	{
		return true;
	}
	return false;
}

void WScene::DekrementacjaFigur(const bool& who)
{
	if(who)
	{
		czarne--;
	}
	else
	{
		biale--;
	}
}

bool WScene::KoniecGry(void) const
{
	if(biale <= 0 || czarne <= 0)
	{
		return true;
	}
	return false;
}

void WScene::Damki(void)
{
	int y[2] = { 0, 7 };
	for(unsigned int k = 0; k < 2; k++)
	{
		for(unsigned int i = 0; i < data.size(); i++)
		{
			Pion analizowany(data[i][y[k]]);
			if(!analizowany.czy_wolny)
			{
				if(!analizowany.czy_damka)
				{
					if(y[k] == 0) // Biale zamieniaja sie w damki
					{
						if(analizowany.player)
						{
							data[i][y[k]].remove("BialyPion");
							data[i][y[k]].push_back("BialaDamka");
						}
					}
					else // czarne sie zamieniaja
					{
						if(!analizowany.player)
						{
							data[i][y[k]].remove("CzarnyPion");
							data[i][y[k]].push_back("CzarnaDamka");
						}
					}
				}
			}
		}
	}
}

std::string WScene::Plansza(void)
{
	std::string output = name;
	if(player == current_player)
	{
		name = Gracz();
	}
	else
	{
		name = AI();
	}
	Draw();
	Damki();
	if(KoniecGry())
	{
		Clock::delay(10000); exit(1);
		// output =
	}
	return output;
}

std::string WScene::Loop(void)
{
	std::string output;
	if(name == "Plansza")
		output = Plansza();
	return output;
}

std::istream& operator >> (std::istream& stream, WScene& temp)
{
	std::string name;
	stream >> name;
	temp.Rename(name);
	
	unsigned int x = 0;
	stream >> x;
	temp.resize(x);
	
	for(unsigned int i = 0; i < x; i++)
	{
		unsigned int y = 0;
		stream >> y;
		for(unsigned int j = 0; j < y; j++)
		{
			unsigned int wage = 0;
			stream >> wage;
			unsigned int SizeX = 0;
			stream >> SizeX;
			unsigned int SizeY = 0;
			stream >> SizeY;
			unsigned int TopX = 0;
			stream >> TopX; 
			unsigned int TopY = 0;
			stream >> TopY;
			Vector<2> Top(TopX,TopY);
			Tile<2> push(Top,wage,SizeX,SizeY);
			temp[i].push_back(push);
		}
	}
	return stream;
}

#endif
