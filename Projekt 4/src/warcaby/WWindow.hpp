#ifndef WWINDOW_HPP
#define WWINDOW_HPP

#include <vector>
#define WindowX 640
#define WindowY 640

class WWindow
{
	protected:
		std::vector<WScene> Scenes;
		sf::RenderWindow oknoAplikacji;
		std::string current_scene;
	public:
		void Loop(void);
		void LoadScene(std::ifstream& stream);
		void Clear(void) { oknoAplikacji.clear(); }
		
		unsigned int Find(const std::string& name)
		{
			for(unsigned int i = 0; i < Scenes.size(); i++)
			{
				if(name == Scenes[i].Name())
				{
					return i;
				}
			}
			return INF;
		} 
		
		WWindow() : oknoAplikacji( sf::VideoMode( WindowX, WindowY, 256 ), "Warcaby"  )
		{
			srand(time(NULL));
			current_scene = "Plansza";
			for(unsigned int i = 0; i < 7000; i++)
			{
				std::ifstream stream;
				stream.open((Directionaries::Scenes + std::to_string(i) + std::string(".txt")).c_str());
				if(!stream.is_open()) break;
				LoadScene(stream);
			}
			Scenes[Find("Plansza")].SetPlayer(true);
		}
};

void WWindow::Loop(void)
{
	//while(oknoAplikacji.isOpen())
	for(unsigned int i = 0;  i < 70000; i++)
	{
		current_scene = ( Scenes[Find(current_scene)].Loop() );
	}
}

void WWindow::LoadScene(std::ifstream& stream)
{
	WScene temp(oknoAplikacji);
	stream >> temp;
	this->Scenes.push_back(temp);

	for(unsigned int i = 0; i < Scenes[Scenes.size()-1].size(); i++)
	{
		for(unsigned int j = 0; j < Scenes[Scenes.size()-1][i].size(); j++)
		{
			unsigned int trash;
			stream >> trash >> trash;
			stream >> Scenes[Scenes.size()-1][i][j];
		}
	}
}

#endif
