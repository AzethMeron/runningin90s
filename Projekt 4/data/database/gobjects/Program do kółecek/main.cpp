#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;
#define WIER 16
#define CenterX 0
#define CenterY 0
#define TILE 80
#define Length 30.0
#define Outline 4 
#define Color 0
#define Fill 1
#define Texture "temp.jpg"
#define Name "kolo"

#define STOPNIE_RADIANY 0.01745329

int main()
{
	ofstream plik;
	plik.open("kolo.txt");
	plik << "1 " << endl;
	plik << CenterX << " " << CenterY << endl << WIER << endl;
	for(unsigned int i = 0; i < WIER; i++)
	{
		float x = Length * cos( float(i)*(float(360)/float(WIER)) * STOPNIE_RADIANY);
		float y = Length * sin( float(i)*(float(360)/float(WIER)) * STOPNIE_RADIANY);
		x = x + TILE/2;
		y = y + TILE/2;
		plik << x << " " << y << " ";
	}
	plik << endl;
	plik << Outline << " " << Color;
	plik << endl;
	plik << Fill;
	plik << endl;
	plik << Texture << " " << Name;
		
	return 0;
}
