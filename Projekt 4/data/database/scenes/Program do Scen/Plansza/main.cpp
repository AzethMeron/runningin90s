#include <iostream>
#include <fstream>

using namespace std;

#define TILESX 8
#define TILESY 8
#define WAGE 0
#define PIXX 80
#define PIXY 80
#define PLANSZA "Plansza"

int main()
{
	ofstream plik;
	plik.open("0.txt");
	
	plik << PLANSZA " ";
	
	/* Generowanie Tilesow */
	plik << TILESX << std::endl;
	for(unsigned int i = 0; i < TILESX; i++)
	{
		plik << TILESY << " ";
		for(unsigned int j = 0; j < TILESY; j++)
		{
			plik << WAGE << " " << PIXX << " " << PIXY << " " << i*PIXX << " " << j*PIXY << " "; // 
		}
		plik << endl;
	}
	
	/* Generowanie Obiektow geometrycznych */
	for(unsigned int i = 0; i < TILESX; i++)
	{
		for(unsigned int j = 0; j < TILESY; j++)
		{
			plik << i << " " << j << " ";
			
			unsigned int temp = (i%2 + j%2)%2;
			if(temp == 0)
			{
				plik << 1;
				//plik << " 1 0 0 4 0 0 80 0 80 80 0 80 2 0 Cream.jpg BialePole "; //  Center LWierzcholkow Koordy Outline Tekstura
				plik << " 0 BialePole ";
			}
			else if(temp == 1)
			{
				if( (j==3) || (j==4) )
				{
					plik << 1;
					plik << " 0 CzarnePole ";
				}
				else
				{
					plik << 2;
					plik << " 0 CzarnePole ";
					if(j < 3)
					{
						plik << "0 CzarnyPion ";
					}
					else
					{
						plik << "0 BialyPion ";
					}
				}
				//plik << " 1 0 0 4 0 0 80 0 80 80 0 80 2 0 Choco.jpg CzarnePole "; //  Not_standard/Standard Center LWierzcholkow Koordy Outline KolorOutline Tekstura
			}
			
			plik << endl;
		}
	}
	
	plik.close();
	return 0;
}
