#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

/* Program stworzony pod Linuxa. 
 * Może nie zadziałać na innych platformach
 * 
 * Wymaga biblioteki SFML:
 * sudo apt install libsfml-dev 
 * 
 * Polecenie do budowania:
 * g++ -Wall -o "%e" "%f" -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio
 * 
 * Zalecane środowisko: Geany
 */

#include "src/engine/engine.hpp"
#include "src/warcaby/warcaby.hpp"

//sf::RenderWindow oknoAplikacji( sf::VideoMode( 640, 640, 256 ), "TEST" );

int main()
{
	Music temp("BoulevardOfBrokenDreams.flac");
	temp.play();
	temp.setLoop(true);
	
	WWindow okno;
	okno.Loop();
	
	return 0;
}
