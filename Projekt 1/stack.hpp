#ifndef GRNA_STACK_HPP
#define GRNA_STACK_HPP
#include <iostream>

/*!
 * \file
 * \brief GRNA Stack header file
 *
 * Include declaration and definitions of GRNA::Stack datatype
 */

namespace GRNA
{
	
	/* *************************************************************************************************************** */
	
	template<typename Type>
	class Stack;
	
	/* *************************************************************************************************************** */
	
	/*! 
	* \brief Vessel to contain datatype and pointer
	* 
	* Vessel to contain datatype and pointer to previous ObjectStack.  
	*/
	template<typename Type>
	class ObjectStack
	{
		friend class Stack<Type>;
		protected:
			Type Current; //!< Contained data
			ObjectStack<Type>* Previous; //!< Pointer to previous ObjectStack
	};
	
	/* *************************************************************************************************************** */
	
	/*! 
	* \brief Stack datatype
	* 
	* Lalalalalalalala
	*/
	template<typename Type>
	class Stack
	{
		protected:
			ObjectStack<Type>* top; //!< Pointer to ObjectStack on top
			unsigned int length; //!< Amount of objects on Stack
		public:
			unsigned int Length(void) const { return this->length; } //!< Amount of objects on Stack
			Type& Top(void)       { return this->top->Current; } //!< Access to object on top
			Type  Top(void) const { return this->top->Current; } //!< Copy of object on top
			Stack() { this->length = 0; top = nullptr; } //!< Constructor. Creates empty stack
			~Stack() { this->Purge(); } //!< Destructor. Free memory
			void Push(const Type& input); 
			Type Pop(void);
			void Purge(void);
	};
	
	/*! 
	* \brief Put data on Stack
	* 
	* Put data on Stack
	* \param[in] input - data you want to put on stack
	*/
	template<typename Type>
	void Stack<Type>::Push(const Type& input)
	{
		ObjectStack<Type>* PreviousTop = this->top;
		this->top = (new ObjectStack<Type>);
		this->Top() = input;
		this->top->Previous = PreviousTop;
		length++;
	}
	
	/*! 
	* \brief Remove data on top
	* 
	* Remove last data from Stack
	* \return Removed data
	*/
	template<typename Type>
	Type Stack<Type>::Pop(void)
	{
		if(length>0)
		{
			ObjectStack<Type>* temp = this->top;
			Type output = this->top->Current;
			this->top = this->top->Previous;
			length--;
			delete temp;
			return output;
		}
		Type output = 0;
		return output;
	}
	
	/*! 
	* \brief Erase all data from Stack
	* 
	* Erase all data from Stack
	*/
	template<typename Type>
	void Stack<Type>::Purge(void)
	{
		while(length)
		{
			this->Pop();
		}
	}
	
	/* *************************************************************************************************************** */
	
}

#endif
