#include <iostream>
#include <stack>
#include <queue>
#include <list>
#include <map>
#include "list.hpp"
#include "stack.hpp"
#include "map.hpp"
#include "queue.hpp"
#include "priorityqueue.hpp"

void TestStack(void) // Zdane
{ /*
	GRNA::Stack<int> Rommel;
	Rommel.Push(5);
	Rommel.Push(7);
	Rommel.Push(9);
	std::cout << Rommel.Pop() << Rommel.Pop() << Rommel.Pop(); */
	
	std::stack<int> k;
	k.push(5);
	k.push(7);
	k.push(9);
	for(int i = 0; i < 3; i++)
	{
		std::cout<<k.top();
		k.pop();
	}	
}

void TestList(void) // Zdane
{
	/*
	GRNA::List<int> xd;
	xd.PushBack(1);
	xd.PushBack(3);
	xd.PushBack(5);
	xd.Remove(3);
	xd.PushFront(-2);
	xd.Insert(9,2);
	std::cout << xd[0] << " " << xd[1] << " " <<xd[2] << " " <<xd[3];
	
	std::cout << std::endl;

	auto temp = xd.End();
	for(unsigned int i = 0; i < 4; i++)
	{
		std::cout << temp.Access() << " ";
		--temp;
	} */
	
	std::list<int> k;
	k.push_back(2);
	k.push_back(4);
	k.push_back(6);
	k.push_back(8);
	k.pop_front();
	k.pop_front();
	for(int& i : k)
	{
		std::cout << i;
	}
}

void TestMap(void) // Zdane
{ 
	/*
	GRNA::Map<std::string,int,100> Rozwadowski;
	Rozwadowski.Insert("ziemia_jest_plaska",10);
	Rozwadowski.Insert("hehe_beka",14);
	Rozwadowski.Insert("lol_frajer",1);
	Rozwadowski.Insert("dlaczego_zyje",99);
	std::cout << Rozwadowski["dlaczego_zyje"] << " ";
	Rozwadowski.Remove("dlaczego_zyje");
	std::cout << Rozwadowski["ziemia_jest_plaska"] << " ";
	std::cout << Rozwadowski["hehe_beka"] << " "; */
	std::map<std::string, int> l;
	l["nie_pluj_do_kurczaka"] = 2;
	l["aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"] = 3;
	std::cout << l["nie_pluj_do_kurczaka"] << l["aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"];
} 

void TestQueue(void) // Zdane
{ /*
	GRNA::Queue<int> Patton;
	Patton.Enqueue(1);
	Patton.Enqueue(3);
	Patton.Enqueue(5);
	Patton.Enqueue(7);
	Patton.Enqueue(9);
	std::cout << Patton.Dequeue() << Patton.Dequeue() << Patton.Dequeue() << Patton.Dequeue() << Patton.Dequeue(); */
	std::queue<int> k;
	k.push(1);
	k.push(2);
	k.push(3);
	k.pop();
	k.pop();
	std::cout << k.front();
}

void TestPriorityQueue(void) // Zdane
{
	/*
	GRNA::PriorityQueue<int> Montgomery; 
	Montgomery.Enqueue(1, 1);
	Montgomery.Enqueue(3, 3);
	Montgomery.Enqueue(5, 5);
	Montgomery.Enqueue(7, 7);
	Montgomery.Enqueue(9, 9);
	std::cout << Montgomery.Dequeue()<< Montgomery.Dequeue()<< Montgomery.Dequeue()<< Montgomery.Dequeue()<< Montgomery.Dequeue();
	Montgomery.Enqueue(9, 1);
	Montgomery.Enqueue(7, 3);
	Montgomery.Enqueue(5, 5);
	Montgomery.Enqueue(3, 7);
	Montgomery.Enqueue(1, 9);
	std::cout << Montgomery.Dequeue()<< Montgomery.Dequeue()<< Montgomery.Dequeue()<< Montgomery.Dequeue()<< Montgomery.Dequeue();
	* */
	std::priority_queue<int,std::vector < int >, std::greater < int >> l;
	l.push(11);
	l.push(2);
	l.push(5);
	for(unsigned int i = 0; i < 3; i++)
	{
		std::cout<<l.top();
		l.pop();
	}
}

int main()
{
	TestStack();
	std::cout<<std::endl;
	TestList();
	std::cout<<std::endl;
	TestMap();
	std::cout<<std::endl;
	TestQueue();
	std::cout<<std::endl;
	TestPriorityQueue();
	std::cout<<std::endl;
	return 0;
}
