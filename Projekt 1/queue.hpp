#ifndef GRNA_QUEUE_HPP
#define GRNA_QUEUE_HPP
#include <list>

namespace GRNA
{
	
	/* *************************************************************************************************************** */
	
	template<typename Type>
	class Queue
	{
		protected:
			std::list<Type> Stored;
		public:
			void Enqueue(const Type& input)
			{
				this->Stored.push_back(input);
			}
			Type Dequeue(void)
			{
				Type output = this->Stored.front();
				this->Stored.pop_front();
				return output;
			}
	};
	
	/* *************************************************************************************************************** */
	
}

#endif
