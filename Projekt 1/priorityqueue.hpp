#ifndef GRNA_PRIORITYQUEUE_HPP
#define GRNA_PRIORITYQUEUE_HPP
#include <list>
#include <vector>

namespace GRNA
{
	
	/* *************************************************************************************************************** */
	
	template<typename Type>
	class ObjectPriorityQueue
	{
		protected:
			std::list<Type> Values;
			unsigned int priority;
		public:
			void SetUp(const unsigned int& Priorit) { this->priority = Priorit; }
			unsigned int Priority(void) const { return this->priority; }
			unsigned int Size(void) const { return this->Values.size(); }
			void Enqueue(const Type& input)
			{
				this->Values.push_back(input);
			}
			Type Dequeue(void)
			{
				Type output = this->Values.front();
				this->Values.pop_front();
				return output;
			}
	};
	
	/* *************************************************************************************************************** */
	
	template<typename Type>
	class PriorityQueue
	{
		private:
			ObjectPriorityQueue<Type>& FindPriority(const unsigned int& Prior)
			{
				for(ObjectPriorityQueue<Type>& i : Stored)
				{
					if(i.Priority() == Prior)
					{
						return i;
					}
				}
			}
			ObjectPriorityQueue<Type>& FindFirst(void)
			{
				for(ObjectPriorityQueue<Type>& i : Stored)
				{
					if(i.Size())
					{
						return i;
					}
				}
			}
		protected:
			std::list<ObjectPriorityQueue<Type>> Stored;
		public:
			void Enqueue(const Type& input, const unsigned int& Priority)
			{
				for(unsigned int i = Stored.size()+1; i <= Priority; i++)
				{
					ObjectPriorityQueue<Type> temp;
					temp.SetUp(i);
					Stored.push_back(temp);
				}
				this->FindPriority(Priority).Enqueue(input);
			}
			Type Dequeue(void)
			{
				return (FindFirst().Dequeue());
			}
	};
	
	/* *************************************************************************************************************** */
	
}

#endif
