#ifndef GRNA_MAP_HPP
#define GRNA_MAP_HPP
#include <vector>
#include <numeric>

/*!
 * \file
 * \brief GRNA Map header file
 *
 * Include declaration and definitions of GRNA::Map datatype
 */

namespace GRNA
{
	
	/* *************************************************************************************************************** */
	
	/*! 
	* \brief Vessel to contain data and key
	* 
	* Vessel to contain data, key and methods to make managment easier.
	* User shouldn't use it.
	*/
	template<typename KeyType, typename DataType>
	class ObjectMap
	{
		private:
			void SetUp(const KeyType& key, const DataType& input)
			{
				this->Value = input;
				this->Key = key;
			}
		protected:
			DataType Value; 
			KeyType Key;
		public:
			bool operator == (const KeyType& key) const
			{
				return (this->Key == key);
			}
			DataType& Access(void)
			{
				return this->Value;
			}
			ObjectMap(const KeyType& key, const DataType& input)
			{
				this->SetUp(key,input);
			}			
	};
	
	/* *************************************************************************************************************** */
	
	/*! 
	* \brief Vessel to contain... more vessels
	* 
	* Vessel to contain lists of ObjectMap, that contains data.
	* User shouldn't use it.
	*/
	template<typename KeyType, typename DataType>
	class ObjectStore
	{
		private:
			ObjectMap<KeyType,DataType>& Find(const KeyType& key)
			{
				for(unsigned int i = 0; i < this->Stored.size(); i++)
				{
					if(Stored[i] == key)
					{ return Stored[i]; }
				}
			}
		protected:
			std::vector<ObjectMap<KeyType,DataType>> Stored;
		public:
			void Insert(const KeyType& key, const DataType& input);
			void Remove(const KeyType& key);
			DataType& Access(const KeyType& key)
			{
				return (this->Find(key)).Access();
			}
	};
	
	template<typename KeyType, typename DataType>
	void ObjectStore<KeyType,DataType>::Insert(const KeyType& key, const DataType& input)
	{
		ObjectMap<KeyType,DataType> temp(key,input);
		this->Stored.push_back(temp);
	}
	
	template<typename KeyType, typename DataType>
	void ObjectStore<KeyType,DataType>::Remove(const KeyType& key)
	{
		this->Find(key) = this->Stored.back();
		this->Stored.pop_back();
	}
	
	/* *************************************************************************************************************** */
	
	/*! 
	* \brief HashMap
	* 
	* Vessel to contain lists of ObjectMap, that contains data.
	* User shouldn't use it.
	*/
	template<typename KeyType, typename DataType, unsigned int Size>
	class Map
	{
		protected:
			ObjectStore<KeyType,DataType> Data[Size];
		public:
			void Insert(const KeyType& key, const DataType& input);
			void Remove(const KeyType& key);
			unsigned int Generator(const KeyType& key) const
			{
				unsigned int output = 0;
				output = std::accumulate(key.begin(),key.end(), 1);
				output = output % Size;
				return output;
			}
			DataType& operator [] (const KeyType& key)
			{
				return (this->Data[Generator(key)]).Access(key);
			}
	};
	
	template<typename KeyType, typename DataType, unsigned int Size>
	void Map<KeyType,DataType,Size>::Insert(const KeyType& key, const DataType& input)
	{
		this->Data[Generator(key)].Insert(key,input);
	}
	
	template<typename KeyType, typename DataType, unsigned int Size>
	void Map<KeyType,DataType,Size>::Remove(const KeyType& key)
	{
		this->Data[Generator(key)].Remove(key);
	}
	
	/* *************************************************************************************************************** */
	
}

#endif
