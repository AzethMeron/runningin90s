#ifndef GRNA_LIST_HPP
#define GRNA_LIST_HPP
#include <iostream>

namespace GRNA
{
	
	/* *************************************************************************************************************** */
	
	template<typename Type>
	class List;
	
	/* *************************************************************************************************************** */
	
	namespace ListWorkspace
	{
		template<typename Type>
		class Iterator;
		
		template<typename Type>
		class ObjectList
		{
			private:
				friend class List<Type>;
				friend class Iterator<Type>;
			protected:
				ObjectList* previous;
				Type current;
				ObjectList* next;
			public:
				ObjectList()
				{
					this->previous = nullptr;
					this->next = nullptr;
				}
				ObjectList(ObjectList* Previous, const Type& Current,  ObjectList* Next)
				{
					this->SetUp(Previous,Current,Next);
				}
				void SetPrev(ObjectList* Previous)
				{
					this->previous = Previous;
				}
				void Set(const Type Current)
				{
					this->current = Current;
				}
				void SetNext(ObjectList* Next)
				{
					this->next = Next;
				}
				void SetUp(ObjectList* Previous, const Type& Current, ObjectList* Next)
				{
					this->previous = Previous;
					this->current = Current;
					this->next = Next;
				}
		};
		
		/* ************************************************************************************************************ */
		
		template<typename Type>
		class Iterator
		{
			private:
				friend class List<Type>;
			protected:
				ListWorkspace::ObjectList<Type>* pointed;
			public:
				Iterator(ListWorkspace::ObjectList<Type>* input)
				{
					this->pointed = input;
				}
				Iterator& operator ++ ()
				{
					pointed = pointed->next;
					return *this;
				}
				Iterator& operator -- ()
				{
					pointed = pointed->previous;
					return *this;
				} 
				Type& Access(void)
				{
					return this->pointed->current;
				}
		};
	}
	
	/* *************************************************************************************************************** */
	
	template<typename Type>
	class List
	{
		private:
			bool ControlLength(const Type& input);
			ListWorkspace::Iterator<Type> FindIndex(const unsigned int& index)
			{
				ListWorkspace::Iterator<Type> temp = this->front;
				for(unsigned int i = 0; i < index; i++)
				{
					++temp;
				}
				return temp;
			}
			ListWorkspace::Iterator<Type> FindInput(const Type& input)
			{
				ListWorkspace::Iterator<Type> temp = this->front;
				for(unsigned int i = 0; i < length; i++)
				{
					if(temp.Access() == input)
					{
						return temp;
					}
					++temp;
				}
			}
		protected:
			ListWorkspace::ObjectList<Type>* front;
			ListWorkspace::ObjectList<Type>* back;
			unsigned int length;
		public:
			void PushFront(const Type& input);
			void PushBack(const Type& input);
			void Insert(const Type& input, const unsigned int& index);
			void Remove(const Type& input);
			void Purge(void);
			ListWorkspace::Iterator<Type> Begin(void) { ListWorkspace::Iterator<Type> temp = this->front; return temp; }
			ListWorkspace::Iterator<Type> End(void) { ListWorkspace::Iterator<Type> temp = this->back; return temp; }
			~List() { this->Purge(); }
			unsigned int Length(void) const { return this->length; }
			Type& operator [] (const unsigned int& index)
			{
				return FindIndex(index).Access();
			}
			List() 
			{ 
				this->front = nullptr; 
				this->back = nullptr; 
				this->length = 0; 
			}
	};
	
	template<typename Type>
	bool List<Type>::ControlLength(const Type& input)
	{
		if(length > 0)
		{
			return true;
		}
		else
		{
			ListWorkspace::ObjectList<Type>* temp = (new ListWorkspace::ObjectList<Type>);
			temp->Set(input);
			this->front = temp;
			this->back = temp;
			return false;
		}
	}
	
	template<typename Type>
	void List<Type>::PushFront(const Type& input)
	{
		if(ControlLength(input))
		{
			ListWorkspace::ObjectList<Type>* temp = (new ListWorkspace::ObjectList<Type> (nullptr,input,this->front));
			this->front->SetPrev(temp);
			this->front = temp;
		}
		length++;
	}
	
	template<typename Type>
	void List<Type>::PushBack(const Type& input)
	{
		if(ControlLength(input))
		{
			ListWorkspace::ObjectList<Type>* temp = (new ListWorkspace::ObjectList<Type> (this->back,input,nullptr));
			this->back->SetNext(temp);
			this->back = temp;
		}
		length++;
	}
	
	template<typename Type>
	void List<Type>::Insert(const Type& input, const unsigned int& index)
	{
		if(index < length)
		{
			ListWorkspace::Iterator<Type> temp = FindIndex(index); // zamiast tego obiektu znajdzie sie nowy, ktory zaraz wstawimy
			ListWorkspace::ObjectList<Type>* NewObject = nullptr;
			{
				// Setup - NewObject
				ListWorkspace::ObjectList<Type>* previous = temp.pointed->previous;
				ListWorkspace::ObjectList<Type>* next = temp.pointed;
				NewObject = (new ListWorkspace::ObjectList<Type> (previous,input,next));
			}
			{
				// Umieszczenie nowego obiektu
				ListWorkspace::ObjectList<Type>* previous = temp.pointed->previous;
				ListWorkspace::ObjectList<Type>* next = temp.pointed;
				previous->SetNext(NewObject);
				next->SetPrev(NewObject);
			}
			length++;
		}
	}
	
	template<typename Type>
	void List<Type>::Remove(const Type& input)
	{
		if(length > 0)
		{
			auto temp = FindInput(input);
			auto previous = temp.pointed->previous;
			auto next = temp.pointed->next;
			previous->next = next;
			next->previous = previous;
			delete temp.pointed;
			length--;
		}
	}
	
	template<typename Type>
	void List<Type>::Purge(void)
	{
		ListWorkspace::Iterator<Type> temp = this->front;
		for(unsigned int i = 0; i < length; i++)
		{
			auto to_deletayshun = temp;
			++temp;
			delete to_deletayshun.pointed;
		}
	}
	
	/* *************************************************************************************************************** */
	
}

#endif
